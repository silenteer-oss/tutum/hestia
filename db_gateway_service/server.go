package db_gateway_service

import (
	"context"
	"time"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/authz"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/mongodb"

	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/dbgateway"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/internal/service"
	"gitlab.com/silenteer-oss/titan"
)

func init() {
	viper.SetDefault("mongodb.uri", "mongodb://127.0.0.1:27017")
	viper.SetDefault("nats.subject", "api.service.db-gateway")
}

// InitDatabaseGateway
// Default parameter:
// mongodb.uri: "mongodb://127.0.0.1:27017"
// nats.subject: "api.service.db-gateway"
func InitDatabaseGateway(dbConfig *mongodb.DbConfig) (*titan.Server, context.CancelFunc) {
	return InitDatabaseGatewayWithPolicy(dbConfig, authz.DbPolicies{})
}

func InitDatabaseGatewayWithPolicy(dbConfig *mongodb.DbConfig, policies authz.DbPolicies) (*titan.Server, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)

	authz := &authz.Authz{DbPolicies: policies}

	service := service.NewDBGatewayWithAuth(ctx, viper.GetString("mongodb.uri"), dbConfig, authz)
	router := dbgateway.NewDbGatewayServiceRouter(service)

	server := titan.NewServer(
		viper.GetString("nats.subject"),
		titan.Routes(router.Register),
	)

	return server, cancel
}
