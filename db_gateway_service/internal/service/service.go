package service

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/authz"

	infra "gitlab.com/silenteer-oss/hestia/infrastructure"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/mongodb"

	"emperror.dev/errors"
	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/dbgateway"
	bsonParser "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/document"
	"gitlab.com/silenteer-oss/titan"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const MultiTenantCollectionIndexReferenceDelimiter = ".none.delete.multitenant.index.template"

type clientSessionMux struct {
	clientSessions map[string]mongo.Session
	mux            sync.Mutex
}

func (session *clientSessionMux) get(key string) (mongo.Session, bool) {
	session.mux.Lock()
	defer session.mux.Unlock()
	result, ok := session.clientSessions[key]
	return result, ok
}

func (session *clientSessionMux) set(key string, sess mongo.Session) {
	session.mux.Lock()
	defer session.mux.Unlock()
	session.clientSessions[key] = sess
}
func (session *clientSessionMux) delete(key string) {
	session.mux.Lock()
	defer session.mux.Unlock()
	delete(session.clientSessions, key)
}
func (session *clientSessionMux) len() int {
	session.mux.Lock()
	defer session.mux.Unlock()
	return len(session.clientSessions)
}

var clientSessions *clientSessionMux

func init() {
	clientSessions = &clientSessionMux{
		clientSessions: make(map[string]mongo.Session),
		mux:            sync.Mutex{},
	}
}

type service struct {
	context  *context.Context
	client   *mongo.Client
	dbConfig *mongodb.DbConfig
	authz    *authz.Authz
}

func getTransactionId(ctx *titan.Context, transactionId *uuid.UUID) string {
	return fmt.Sprintf("%s_%s", ctx.RequestId(), transactionId.String())
}

func (service *service) OpenTransaction(ctx *titan.Context, transaction *dbgateway.Transaction) (err error) {
	transactionId := getTransactionId(ctx, transaction.TransactionId)

	session, notOK := clientSessions.get(transactionId)

	if notOK {
		return errors.New(fmt.Sprintf("duplicate.transaction.id: %s", transactionId))
	}

	if session, err = service.client.StartSession(); err != nil {
		return errors.WithStack(err)
	} else if err = session.StartTransaction(); err == nil {
		clientSessions.set(transactionId, session)
	}

	// if there is a error, clean the resources.
	if err != nil {
		clientSessions.delete(transactionId)
		if session != nil {
			session.EndSession(ctx)
		}
		return errors.WithStack(err)
	}
	return nil
}

func (service *service) CommitTransaction(ctx *titan.Context, transaction *dbgateway.Transaction) (err error) {

	transactionId := getTransactionId(ctx, transaction.TransactionId)
	session, ok := clientSessions.get(transactionId)
	if !ok {
		return errors.New(fmt.Sprintf("transaction not exist %s", transaction.TransactionId))
	}

	if err = session.CommitTransaction(ctx); err == nil {
		session.EndSession(ctx)
		clientSessions.delete(transactionId)
	}
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (service *service) AbortTransaction(ctx *titan.Context, transaction *dbgateway.Transaction) error {
	transactionId := getTransactionId(ctx, transaction.TransactionId)

	session, ok := clientSessions.get(transactionId)
	if !ok {
		return errors.New(fmt.Sprintf("transaction not exist %s", transaction.TransactionId))
	}
	err := session.AbortTransaction(ctx)
	clientSessions.delete(transactionId)
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (service *service) CloseTransaction(ctx *titan.Context, transaction *dbgateway.Transaction) (err error) {
	transactionId := getTransactionId(ctx, transaction.TransactionId)

	session, ok := clientSessions.get(transactionId)
	if !ok {
		return errors.New(fmt.Sprintf("transaction not exist %s", transaction.TransactionId))
	}
	session.EndSession(ctx)
	clientSessions.delete(transactionId)
	return nil
}

func (service *service) Collections(ctx *titan.Context, request *dbgateway.CollectionRequest) (*dbgateway.CollectionResponse, error) {
	result := &dbgateway.CollectionResponse{}

	filter := bson.D{
		{
			Key: "name",
			Value: bson.D{
				{
					Key:   "$regex",
					Value: primitive.Regex{Pattern: fmt.Sprintf("^%s*\\.", request.CollectionName), Options: "i"},
				},
			},
		},
	}
	opts := options.ListCollections().SetNameOnly(true)
	cursor, err := service.client.Database(request.DatabaseName).ListCollections(ctx, filter, opts)
	if err == nil {
		defer func() {
			cursor.Close(ctx)
		}()

		for cursor.Next(ctx) {
			result.Names = append(result.Names, cursor.Current.Index(0).Value().StringValue())
		}
	} else {
		return result, errors.WithStack(err)
	}
	return result, err
}

func (service *service) FindOneAndReplace(ctx *titan.Context, request *dbgateway.FindOneAndReplaceRequest) (*dbgateway.FindOneAndReplaceResponse, error) {
	result := &dbgateway.FindOneAndReplaceResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filter, err := service.authz.IsolateDataContextForFindOneAndReplace(ctx, request)
	if err != nil {
		return nil, err
	}

	opts := options.FindOneAndReplace()

	if request.ReturnNewDocument != nil {
		returnDocument := options.ReturnDocument(*request.ReturnNewDocument)
		opts.ReturnDocument = &returnDocument
	}

	if request.Upsert != nil {
		opts.Upsert = request.Upsert
	}
	session, err := service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}

	var bsonDocument bson.Raw
	err = document.Entity(request.Document, &bsonDocument)
	if err != nil {
		return result, errors.WithStack(err)
	}

	if session == nil {
		if resultQuery := collection.FindOneAndReplace(ctx, filter, bsonDocument, opts); resultQuery.Err() == nil {
			updatedDoc, err := resultQuery.DecodeBytes()
			if err == nil {
				if doc, err := bsonParser.Byte(updatedDoc); err == nil {
					result.Document = doc
				}
			}
		} else {
			err = resultQuery.Err()
		}
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			if resultQuery := collection.FindOneAndReplace(ctx, filter, bsonDocument, opts); resultQuery.Err() == nil {
				updatedDoc, err := resultQuery.DecodeBytes()
				if err == nil {
					if doc, err := bsonParser.Byte(updatedDoc); err == nil {
						result.Document = doc
					}
				}
			} else {
				err = resultQuery.Err()
			}
			return err
		})
	}
	if err != nil {
		err = errors.WithStack(err)
	}
	return result, err
}

func (service *service) FindOneAndUpdate(ctx *titan.Context, request *dbgateway.FindOneUpdateRequest) (*dbgateway.FindOneUpdateResponse, error) {
	result := &dbgateway.FindOneUpdateResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filter, err := service.authz.IsolateDataContextForFindOneAndUpdate(ctx, request)
	if err != nil {
		return nil, err
	}

	var opts options.FindOneAndUpdateOptions
	if request.Option != nil {
		err = bsonParser.ToEntity(*request.Option, &opts)
	}
	if request.Upsert != nil {
		opts.Upsert = request.Upsert
	}
	if request.ReturnNewDocument != nil {
		r := options.ReturnDocument(*request.ReturnNewDocument)
		opts.ReturnDocument = &r
	}

	if err != nil {
		return result, errors.WithStack(err)
	}
	var session mongo.Session
	if session, err = service.getSession(ctx, request.TransactionId); err != nil {
		return result, errors.WithStack(err)
	}
	var updateResult *mongo.SingleResult

	if session == nil {
		updateResult = collection.FindOneAndUpdate(ctx, filter, request.Update, &opts)
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			updateResult = collection.FindOneAndUpdate(sc, filter, request.Update, &opts)
			return nil
		})
		if err != nil {
			return result, errors.WithStack(err)
		}
	}
	result.Document, err = updateResult.DecodeBytes()
	if err != nil && err.Error() != mongo.ErrNoDocuments.Error() {
		return result, errors.WithStack(err)
	}
	return result, nil
}

func (service *service) UpdateMany(ctx *titan.Context, request *dbgateway.UpdateManyRequest) (*dbgateway.UpdateManyResponse, error) {
	result := &dbgateway.UpdateManyResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	opts := &options.UpdateOptions{
		ArrayFilters:             nil,
		BypassDocumentValidation: nil,
		Collation:                nil,
		Upsert:                   request.Upsert,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filter, err := service.authz.IsolateDataContextForUpdateMany(ctx, request)
	if err != nil {
		return nil, err
	}

	var session mongo.Session
	session, err = service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}
	var updateResult *mongo.UpdateResult

	if session == nil {
		updateResult, err = collection.UpdateMany(ctx, filter, request.Update, opts)
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			updateResult, err = collection.UpdateMany(ctx, filter, request.Update, opts)
			return err
		})
	}
	if err != nil {
		return result, errors.WithStack(err)
	}

	if updateResult.ModifiedCount > 0 {
		result.ModifiedCount = updateResult.ModifiedCount
	} else if updateResult.UpsertedCount > 0 {
		result.ModifiedCount = updateResult.UpsertedCount
	}

	// if the client request return the modified docs
	if request.ReturnModified != nil && *request.ReturnModified {
		// if the filter does not match any documents, and Upsert is true
		if updateResult.UpsertedID != nil {
			filterByte, _ := bsonParser.Byte(bson.D{{
				Key:   "_id",
				Value: updateResult.UpsertedID,
			}})
			resultQuery, err := service.find(ctx, collection, session, filterByte)
			if err == nil {
				result.Documents, err = document.Bytes(resultQuery)
			}
			if err != nil {
				return result, errors.WithStack(err)
			}
		} else if updateResult.ModifiedCount > 0 {
			resultQuery, err := service.find(ctx, collection, session, filter)
			if err == nil {
				result.Documents, err = document.Bytes(resultQuery)
			}
			if err != nil {
				return result, errors.WithStack(err)
			}
		}
	}
	if err != nil {
		err = errors.WithStack(err)
	}
	return result, err

}

func (service *service) Update(ctx *titan.Context, request *dbgateway.UpdateRequest) (*dbgateway.UpdateResponse, error) {
	result := &dbgateway.UpdateResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	f, err := service.authz.IsolateDataForUpdate(ctx, request)
	if err != nil {
		return nil, err
	}

	var session mongo.Session
	session, err = service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}

	var bsonDocuments []bson.Raw

	err = document.Entities(request.Documents, &bsonDocuments)
	opts := options.FindOneAndReplace().SetReturnDocument(options.After)
	if err != nil {
		return result, errors.WithStack(err)
	}
	if session == nil {
		for _, doc := range bsonDocuments {
			filter := f(bson.D{
				{
					Key:   "_id",
					Value: doc.Lookup("_id"),
				},
			})

			resultQuery := collection.FindOneAndReplace(ctx, filter, doc, opts)

			if err = resultQuery.Err(); err == nil {
				updatedDoc, err := resultQuery.DecodeBytes()
				if err == nil {
					if doc, err := bsonParser.Byte(updatedDoc); err == nil {
						result.Documents = append(result.Documents, doc)
					}
				}
			}
		}
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			for _, doc := range bsonDocuments {
				filter := f(bson.D{
					{
						Key:   "_id",
						Value: doc.Lookup("_id"),
					},
				})
				resultQuery := collection.FindOneAndReplace(sc, filter, doc, opts)

				if err = resultQuery.Err(); err == nil {
					updatedDoc, err := resultQuery.DecodeBytes()
					if err == nil {
						if doc, err := bsonParser.Byte(updatedDoc); err == nil {
							result.Documents = append(result.Documents, doc)
						}
					}
				}
			}
			if err != nil {
				err = errors.WithStack(err)
			}
			return err
		})

	}
	if err != nil {
		err = errors.WithStack(err)
	}

	return result, err
}

func (service *service) Count(ctx *titan.Context, request *dbgateway.CountRequest) (*dbgateway.CountResponse, error) {
	result := &dbgateway.CountResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filter, err := service.authz.IsolateDataContextForCount(ctx, request)

	if err != nil {
		return nil, err
	}

	var session mongo.Session
	session, err = service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}
	if session == nil {
		result.Count, err = collection.CountDocuments(ctx, filter)
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			result.Count, err = collection.CountDocuments(sc, filter)
			return err
		})
	}
	if err != nil {
		err = errors.WithStack(err)
	}
	return result, err
}

func (service *service) Aggregate(ctx *titan.Context, request *dbgateway.AggregateRequest) (*dbgateway.AggregateResponse, error) {
	result := &dbgateway.AggregateResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filters, err := service.authz.IsolateDataContextForAggregate(ctx, request)

	if err != nil {
		return nil, err
	}

	var resultQuery []interface{}
	var session mongo.Session
	session, err = service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}

	if session == nil {
		var cursor *mongo.Cursor
		cursor, err = collection.Aggregate(ctx, filters)
		if err == nil {
			defer func() {
				cursor.Close(ctx)
			}()
			for cursor.Next(ctx) {
				resultQuery = append(resultQuery, cursor.Current)
			}
			result.Documents, err = document.Bytes(resultQuery)
		}
		if err != nil {
			err = errors.WithStack(err)
		}
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			cursor, err := collection.Aggregate(sc, filters)
			if err == nil {
				defer func() {
					cursor.Close(sc)
				}()
				for cursor.Next(sc) {
					resultQuery = append(resultQuery, cursor.Current)
				}
				result.Documents, err = document.Bytes(resultQuery)
			}
			return err
		})
		if err != nil {
			err = errors.WithStack(err)
		}
	}
	return result, err
}

func NewDBGateway(context context.Context, uri string, dbConfig *mongodb.DbConfig) dbgateway.DbGatewayService {
	return NewDBGatewayWithAuth(context, uri, dbConfig, &authz.Authz{})
}

func NewDBGatewayWithAuth(context context.Context, uri string, dbConfig *mongodb.DbConfig, authz *authz.Authz) dbgateway.DbGatewayService {
	options := options.Client().ApplyURI(uri)
	fa := false
	options.RetryWrites = &fa
	client, err := mongo.Connect(context, options)
	if err == nil {
		err = client.Ping(context, readpref.Primary())
	}
	if err != nil {
		panic(errors.WithStack(err))
	}
	return &service{
		context:  &context,
		client:   client,
		dbConfig: dbConfig,
		authz:    authz,
	}
}

func (service *service) Create(ctx *titan.Context, request *dbgateway.CreateRequest) (*dbgateway.CreateResponse, error) {
	result := &dbgateway.CreateResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	err = service.authz.CheckDataContextIsolationForCreate(ctx, request)
	if err != nil {
		return nil, err
	}

	var session mongo.Session
	var bsonDocument []interface{}

	err = document.Entities(request.Documents, &bsonDocument)
	if err != nil {
		return result, errors.WithStack(err)
	}
	if session, err = service.getSession(ctx, request.TransactionId); err != nil {
		return result, errors.WithStack(err)
	}

	if session == nil {
		_, err = collection.InsertMany(ctx, bsonDocument)
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			_, err = collection.InsertMany(sc, bsonDocument)
			if err != nil {
				err = errors.WithStack(err)
			}
			return err
		})
	}
	if err == nil {
		parsedDocument, err := document.Bytes(bsonDocument)
		if err == nil {
			result.Documents = parsedDocument
		} else {
			err = errors.WithStack(err)
		}
	}
	return result, err
}

func (service *service) FindOne(ctx *titan.Context, request *dbgateway.FindOneRequest) (*dbgateway.FindOneResponse, error) {
	result := &dbgateway.FindOneResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filter, err := service.authz.IsolateDataContextForFindOne(ctx, request)

	if err != nil {
		return nil, err
	}
	optionFind := options.FindOne()

	// In Find function, there is a parser to parse byte into BRaw, so it does not need to parse by us.
	if request.Projection != nil {
		optionFind.SetProjection(*request.Projection)
	}

	if request.Sort != nil {
		optionFind.SetSort(*request.Sort)
	}

	if request.Skip != nil && *request.Skip > 0 {
		optionFind.SetSkip(*request.Skip)
	}

	session, err := service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}
	resultQuery, err := service.findOne(ctx, collection, session, filter, optionFind)
	if err != nil {
		return result, errors.WithStack(err)
	}
	if resultQuery != nil {
		result.Document, err = bsonParser.Byte(resultQuery)
	}
	if err != nil {
		err = errors.WithStack(err)
	}
	return result, err
}

func (service *service) Read(ctx *titan.Context, request *dbgateway.ReadRequest) (*dbgateway.ReadResponse, error) {
	result := &dbgateway.ReadResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filter, err := service.authz.IsolateDataContextForRead(ctx, request)
	if err != nil {
		return nil, err
	}

	optionFind := options.Find()

	// In Find function, there is a parser to parse byte into BRaw, so it does not need to parse by us.
	if request.Projection != nil {
		optionFind.SetProjection(*request.Projection)
	}

	if request.Sort != nil {
		optionFind.SetSort(*request.Sort)
	}

	if request.Skip != nil && *request.Skip > 0 {
		optionFind.SetSkip(*request.Skip)
	}

	if request.Limit != nil && *request.Limit > 0 {
		optionFind.SetLimit(*request.Limit)
	}

	session, err := service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}
	resultQuery, err := service.find(ctx, collection, session, filter, optionFind)
	if err != nil {
		return result, errors.WithStack(err)
	}
	result.Documents, err = document.Bytes(resultQuery)
	if err != nil {
		err = errors.WithStack(err)
	}
	return result, err
}

func (service *service) Delete(ctx *titan.Context, request *dbgateway.DeleteRequest) (*dbgateway.DeleteResponse, error) {
	result := &dbgateway.DeleteResponse{
		DatabaseName:   request.DatabaseName,
		CollectionName: request.CollectionName,
		TransactionId:  request.TransactionId,
	}

	collection, err := service.getTenantCollection(ctx, request.DatabaseName, request.CollectionName)
	if err != nil {
		return nil, err
	}

	filter, err := service.authz.IsolateDataContextForDelete(ctx, request)

	if err != nil {
		return nil, err
	}

	var session mongo.Session
	session, err = service.getSession(ctx, request.TransactionId)
	if err != nil {
		return result, errors.WithStack(err)
	}

	var resultDelete *mongo.DeleteResult
	if session == nil {
		resultDelete, err = collection.DeleteMany(ctx, filter)
		if err != nil {
			err = errors.WithStack(err)
		}
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			resultDelete, err = collection.DeleteMany(sc, filter)
			if err != nil {
				err = errors.WithStack(err)
			}
			return err
		})
	}
	if err == nil && resultDelete != nil {
		result.DeletedCount = resultDelete.DeletedCount
	}
	return result, err
}

func (service *service) getSession(ctx *titan.Context, requestId *uuid.UUID) (session mongo.Session, err error) {
	if requestId == nil {
		return nil, nil
	}
	transactionId := getTransactionId(ctx, requestId)
	session, ok := clientSessions.get(transactionId)
	if !ok {
		err = errors.New(fmt.Sprintf("transaction not exist %s", requestId))
	}
	return session, err
}
func (service *service) findOne(ctx *titan.Context, collection *mongo.Collection, session mongo.Session, filter []byte, opts ...*options.FindOneOptions) (bson.Raw, error) {
	var resultQuery bson.Raw
	var err error

	if session == nil {
		cursor := collection.FindOne(ctx, filter, opts...)
		if cursor != nil && cursor.Err() == nil {
			resultQuery, err = cursor.DecodeBytes()
		}
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			cursor := collection.FindOne(ctx, filter, opts...)
			if cursor != nil && cursor.Err() == nil {
				resultQuery, err = cursor.DecodeBytes()
			}
			return err
		})
	}
	return resultQuery, err
}
func (service *service) find(ctx *titan.Context, collection *mongo.Collection, session mongo.Session, filter []byte, opts ...*options.FindOptions) ([]bson.Raw, error) {
	resultQuery := make([]bson.Raw, 0)
	var err error

	if session == nil {
		cursor, err := collection.Find(ctx, filter, opts...)
		if err != nil {
			err = errors.WithStack(err)
		}
		if err == nil {
			defer func() {
				cursor.Close(ctx)
			}()
			for cursor.Next(ctx) {
				resultQuery = append(resultQuery, cursor.Current)
			}
		}
	} else {
		err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
			cursor, err := collection.Find(sc, filter, opts...)
			if err != nil {
				err = errors.WithStack(err)
			}
			if err == nil {
				defer func() {
					cursor.Close(sc)
				}()
				for cursor.Next(sc) {
					resultQuery = append(resultQuery, cursor.Current)
				}
			}
			return err
		})
	}
	return resultQuery, err
}

func (service *service) CreateMultiTenantCollections(ctx *titan.Context, request *dbgateway.MultiTenantCollectionCreateRequest) error {
	dbResult, err := service.client.ListDatabases(ctx, bson.D{})
	if err != nil {
		return errors.WithStack(err)
	}

	for _, dbName := range dbResult.Databases {
		db := service.client.Database(dbName.Name)
		cols, err := db.ListCollectionNames(ctx, bson.D{})
		if err != nil {
			return errors.WithStack(err)
		}

		for _, col := range cols {
			if index := strings.Index(col, MultiTenantCollectionIndexReferenceDelimiter); index > 0 {
				collectionName := fmt.Sprintf("%v.%v", col[:index], request.MultiTenantId.String())
				ctx.Logger().Info("Create tenant for ", map[string]interface{}{"collection": collectionName, "db": dbName.Name})

				indexView := db.Collection(col).Indexes()

				cursor, _ := indexView.List(ctx)
				indexModels := make([]mongo.IndexModel, 0)
				for cursor.Next(ctx) {
					indexModel, err := createIndexModel(cursor.Current)
					if err != nil {
						return errors.WithStack(err)
					}
					indexModels = append(indexModels, indexModel)
				}
				_, e := db.Collection(collectionName).Indexes().CreateMany(ctx, indexModels)
				if e != nil {
					return errors.WithStack(e)
				}
			}
		}

	}

	return nil
}

func createIndexModel(indexDocument bson.Raw) (mongo.IndexModel, error) {
	indexOption := &options.IndexOptions{}
	var result mongo.IndexModel
	if val, err := indexDocument.LookupErr("key"); err != nil {
		return result, errors.New("createIndexModel.key is empty")
	} else {
		result.Keys = val.Document()
	}
	if version := indexDocument.Lookup("v"); version.Type == bson.TypeInt32 {
		indexOption.SetVersion(version.Int32())
	}

	if name := indexDocument.Lookup("name"); name.Type == bson.TypeString {
		indexOption.SetName(name.String())
	}
	if unique := indexDocument.Lookup("unique"); unique.Type == bson.TypeBoolean {
		indexOption.SetUnique(unique.Boolean())
	}
	if expireAfterSeconds := indexDocument.Lookup("expireAfterSeconds"); expireAfterSeconds.Type == bson.TypeInt32 {
		indexOption.SetExpireAfterSeconds(expireAfterSeconds.Int32())
	}
	if background := indexDocument.Lookup("background"); background.Type == bson.TypeBoolean {
		indexOption.SetBackground(background.Boolean())
	}
	if defaultLanguage := indexDocument.Lookup("default_language"); defaultLanguage.Type == bson.TypeString {
		indexOption.SetDefaultLanguage(defaultLanguage.String())
	}
	if languageOverride := indexDocument.Lookup("language_override"); languageOverride.Type == bson.TypeString {
		indexOption.SetLanguageOverride(languageOverride.String())
	}
	if textVersion := indexDocument.Lookup("textIndexVersion"); textVersion.Type == bson.TypeInt32 {
		indexOption.SetTextVersion(textVersion.Int32())
	}
	if weights, err := indexDocument.LookupErr("weights"); err == nil {
		indexOption.SetWeights(weights.Document())
	}

	if collation, err := indexDocument.LookupErr("collation"); err == nil {
		parsedCollation := options.Collation{}
		err = collation.Unmarshal(&parsedCollation)
		if err == nil {
			indexOption.SetCollation(&parsedCollation)
		}
	}
	if partialFilterExpression, err := indexDocument.LookupErr("partialFilterExpression"); err == nil {
		indexOption.SetPartialFilterExpression(partialFilterExpression.Document())
	}
	result.Options = indexOption
	return result, nil
}

func (service *service) checkTenantCollectionAccessPermission(ctx *titan.Context, dbName string, collectionName string) (string, error) {
	var role titan.Role
	if ctx.UserInfo() != nil {
		role = ctx.UserInfo().Role
	}

	// don't check admin and system role
	if role == infra.SYS_ADMIN || role == infra.SYSTEM {
		return collectionName, nil
	}

	collectionNameWithoutTenant := collectionName
	if lastIndex := strings.LastIndex(collectionName, "."); lastIndex > 0 {
		collectionNameWithoutTenant = collectionName[:lastIndex]
	}

	col := service.dbConfig.GetCollection(dbName, collectionNameWithoutTenant)
	if col == nil {
		//ignore this case to compatible with old java code because not all java's tables were migrated to Go
		return collectionName, nil
		//return "", errors.New(fmt.Sprintf("Db '%s', collection '%s' does not exist", dbName, collectionName))
	}

	if col.IsTenant {
		if ctx == nil || ctx.UserInfo() == nil || ctx.UserInfo().CareProviderId == "" {
			errMessage := fmt.Sprintf("user information or care provider id is empty, db=%s, collection = %s, role=%s", dbName, collectionName, role)
			ctx.Logger().Error(errMessage)
			return "", errors.New(errMessage)
		}

		if !strings.HasSuffix(collectionName, ctx.UserInfo().CareProviderId.String()) {
			errMessage := fmt.Sprintf("cross tenant access violation, db=%s,colllection=%s,role=%s", collectionName, ctx.UserInfo().CareProviderId.String(), role)
			ctx.Logger().Error(errMessage)
			return "", errors.New(errMessage)
		}
	}

	return collectionName, nil
}

//todo:in the future we will implement logic to map tenant to real db in here
func (service *service) getTenantDatabaseName(ctx *titan.Context, dbName string) (string, error) {
	return dbName, nil
}

func (service *service) getTenantCollection(ctx *titan.Context, dbName string, collectionName string) (*mongo.Collection, error) {
	tenantCollectionName, err := service.checkTenantCollectionAccessPermission(ctx, dbName, collectionName)
	if err != nil {
		return nil, err
	}

	tenantDb, err := service.getTenantDatabaseName(ctx, dbName)
	if err != nil {
		return nil, err
	}

	collection := service.client.Database(tenantDb).Collection(tenantCollectionName)
	return collection, nil
}
