package model

type SequenceNumber struct {
	CollectionName           string `bson:"collectionName"`
	CollectionSequenceNumber int64  `bson:"collectionSequenceNumber"`
}
