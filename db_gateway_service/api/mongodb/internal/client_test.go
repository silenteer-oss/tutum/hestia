package internal_test

import (
	"context"
	"fmt"
	"os"
	"sort"
	"testing"
	"time"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/mongodb"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/dbgateway"

	"github.com/google/go-cmp/cmp"
	"github.com/google/uuid"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/internal/service"
	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/document"
	"gitlab.com/silenteer-oss/titan"
	bson "go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var db struct {
	Client  mongodb.IDBClient
	Context *titan.Context
}

type Person struct {
	Id       *uuid.UUID `json:"_id,omitempty" bson:"_id,omitempty"`
	Index    int        `json:"index" bson:"index"` //for testing sort
	Name     string     `json:"name" bson:"name"`
	Phone    string     `json:"phone" bson:"phone"`
	ActionId string     `json:"actionId" bson:"actionId"`
	Friends  *[]Friend  `json:"friends,omitempty" bson:"friends"`
}

type PartialPerson struct {
	Name    string    `bson:"name"`
	Phone   string    `bson:"phone,omitempty"`   // for testing zero value with omitempty
	Address string    `bson:"address"`           // for testing new added field.
	Friends *[]Friend `bson:"friends,omitempty"` // for testing nil pointer with omitempty
}
type FullPerson struct {
	Id       *uuid.UUID `json:"_id,omitempty" bson:"_id,omitempty"`
	Index    int        `json:"index" bson:"index"` //for testing sort
	Name     string     `json:"name" bson:"name"`
	Phone    string     `json:"phone" bson:"phone"`
	ActionId string     `json:"actionId" bson:"actionId"`
	Friends  *[]Friend  `json:"friends,omitempty" bson:"friends"`
	Address  string     `bson:"address"`
}

type PersonGroup struct {
	Name          string `json:"name" bson:"_id"`
	NumberFriends int    `json:"index" bson:"numberFriends"`
}

type Friend struct {
	ID    *uuid.UUID `json:"friendId,omitempty" bson:"friendId,omitempty"`
	Index int        `json:"index" bson:"index"` //for testing sort
	Name  string     `json:"name" bson:"name"`
	Phone string     `json:"phone" bson:"phone"`
}

var collectionName string

func init() {
	collectionName = uuid.New().String()
	fmt.Println(collectionName)
}

func NewDBGateWay() (*titan.Server, context.CancelFunc) {
	viper.SetDefault("mongodb.uri", "mongodb://127.0.0.1:27017/?retryWrites=false")
	viper.SetDefault("nats.subject", "api.service.db-gateway")

	dbConfig := mongodb.NewDbConfig()
	testDb := mongodb.NewDatabase("test_db", map[string]bool{
		collectionName: false,
	})
	dbConfig.Add(testDb)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)

	service := service.NewDBGateway(ctx, viper.GetString("mongodb.uri"), dbConfig)
	router := dbgateway.NewDbGatewayServiceRouter(service)

	server := titan.NewServer(
		viper.GetString("nats.subject"),
		titan.Routes(router.Register),
	)
	return server, cancel

}

func TestMain(m *testing.M) {
	natClient := titan.GetDefaultClient()
	db.Client = mongodb.InitDbClient(natClient, "test_db", collectionName, false)
	db.Context = titan.NewBackgroundContext()
	server, cancel := NewDBGateWay()

	ch := make(chan interface{}, 1)
	go func() {
		server.Start(ch)
	}()

	select {
	case <-ch:
	case <-time.After(time.Duration(2) * time.Second):
		fmt.Println(fmt.Sprintf("Servers start timed out after %d seconds", 2))
		os.Exit(1)
	}

	// before
	mrun := m.Run()
	//
	cancel()
	server.Stop()

	// after
	os.Exit(mrun)
}

func initCreateModel(numberData int, actionId string) []Person {
	var createDocs []Person

	for ; numberData > 0; numberData-- {
		id, _ := uuid.NewRandom()

		var friends []Friend
		for i := numberData; i > 0; i-- {
			friendId, _ := uuid.NewRandom()
			friends = append(friends, Friend{
				ID:    &friendId,
				Index: i,
				Name:  fmt.Sprintf("Friend name %d", i),
				Phone: fmt.Sprintf("0000%d11", i),
			})
		}

		createDocs = append(createDocs, Person{
			Id:       &id,
			Name:     fmt.Sprintf("Thanh%d", numberData),
			Index:    numberData,
			Phone:    fmt.Sprintf("0000%d22", numberData),
			ActionId: actionId,
			Friends:  &friends,
		})

	}
	return createDocs
}

func TestCreate(t *testing.T) {
	var response Person
	request := initCreateModel(1, "TestCreate")[0]
	err := db.Client.Create(db.Context, request, &response)
	assert.Nil(t, err)
	assert.True(t, cmp.Equal(request, response))
}

//
func TestCreateMany(t *testing.T) {

	var receiver []Person
	request := initCreateModel(10, "TestCreateMany")
	err := db.Client.Create(db.Context, request, &receiver)
	assert.Nil(t, err)
	assert.NotEmpty(t, receiver)
	assert.Equal(t, cmp.Equal(request, receiver), true)

}

func TestUpdate(t *testing.T) {

	request, _, _ := PrepareData(t, 1, "TestUpdate")
	data := request[0]

	update := Person{
		Id:       data.Id,
		Index:    data.Index + 1,
		Name:     fmt.Sprintf("Updated %s", data.Name),
		Phone:    fmt.Sprintf("33 %s", data.Phone),
		ActionId: data.ActionId,
		Friends:  data.Friends,
	}

	var response Person

	err := db.Client.Update(db.Context, update, &response)
	assert.Nil(t, err)
	assert.Equal(t, cmp.Equal(update, response), true)
}

func TestUpdates(t *testing.T) {

	requested, _, _ := PrepareData(t, 10, "TestUpdates")
	var updateList []Person
	for _, data := range requested {
		updateList = append(updateList, Person{
			Id:       data.Id,
			Index:    data.Index + 1,
			Name:     fmt.Sprintf("Updated %s", data.Name),
			Phone:    fmt.Sprintf("33 %s", data.Phone),
			ActionId: data.ActionId,
			Friends:  data.Friends,
		})
	}

	var response []Person
	err := db.Client.Update(db.Context, updateList, &response)
	assert.Nil(t, err)
	assert.Equal(t, cmp.Equal(updateList, response), true)
}

func TestFindOneAndUpdate(t *testing.T) {
	if _, _, err := PrepareData(t, 10, "TestFindOneAndUpdate"); err != nil {
		t.Fatalf("Eror: %v", err)
	}

	// Find One And Update with return Document
	filter := bson.D{{Key: "actionId", Value: "TestFindOneAndUpdate"}}
	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "phone", Value: "1000000001"},
		}},
	}
	t.Log("Find One And Update with return Document")

	var result1 Person
	returnDoc := options.After
	ops := options.FindOneAndUpdateOptions{
		ArrayFilters:             nil,
		BypassDocumentValidation: nil,
		Collation:                nil,
		MaxTime:                  nil,
		Projection:               nil,
		ReturnDocument:           &returnDoc,
		Sort:                     nil,
		Upsert:                   nil,
	}

	err := db.Client.FindOneAndUpdate(db.Context, filter, update, &result1, &ops)

	assert.Nil(t, err)
	assert.NotNil(t, result1)
	assert.Equal(t, "1000000001", result1.Phone)

	//Find One And Update with Upsert = false
	t.Log("Find One And Update with Upsert = false")

	var result2 Person
	filter2 := bson.D{{Key: "actionId", Value: "TestFindOneAndUpdate_NOTFOUND"}}
	update2 := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "phone", Value: "2000000001"},
			{Key: "Name", Value: "notfound"},
		}},
	}
	upsert := false
	ops2 := options.FindOneAndUpdateOptions{
		ArrayFilters:             nil,
		BypassDocumentValidation: nil,
		Collation:                nil,
		MaxTime:                  nil,
		Projection:               nil,
		ReturnDocument:           &returnDoc,
		Sort:                     nil,
		Upsert:                   &upsert,
	}
	err = db.Client.FindOneAndUpdate(db.Context, filter2, update2, &result2, &ops2)
	fmt.Println(err)
	assert.Nil(t, err)
	assert.Empty(t, result2)

	//Find One And Update with Upsert = true
	t.Log("Find One And Update with Upsert = true")
	var result3 Person
	newId, _ := uuid.NewRandom()
	filter3 := bson.D{{Key: "actionId", Value: "TestFindOneAndUpdate_NOTFOUND"}}
	update3 := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "phone", Value: "2000000001"},
			{Key: "Name", Value: "notfound"},
		}},
		{Key: "$setOnInsert", Value: bson.D{
			{Key: "_id", Value: newId},
		}},
	}
	upsert3 := true
	ops3 := options.FindOneAndUpdateOptions{
		ArrayFilters:             nil,
		BypassDocumentValidation: nil,
		Collation:                nil,
		MaxTime:                  nil,
		Projection:               nil,
		ReturnDocument:           &returnDoc,
		Sort:                     nil,
		Upsert:                   &upsert3,
	}
	err = db.Client.FindOneAndUpdate(db.Context, filter3, update3, &result3, &ops3)
	assert.Nil(t, err)
	assert.NotEmpty(t, result3)
	assert.Equal(t, "2000000001", result3.Phone)
	assert.Equal(t, "notfound", result3.Name)

	// Find One And Update with Order
	t.Log("Find One And Update with Order")

	filter4 := bson.D{{Key: "actionId", Value: "TestFindOneAndUpdate"}}
	update4 := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "phone", Value: "4000000001"},
		}},
	}

	sort4 := bson.D{
		{Key: "index", Value: -1},
	}
	var result4 Person
	returnDoc4 := options.After
	ops4 := options.FindOneAndUpdateOptions{
		ArrayFilters:             nil,
		BypassDocumentValidation: nil,
		Collation:                nil,
		MaxTime:                  nil,
		Projection:               nil,
		ReturnDocument:           &returnDoc4,
		Sort:                     sort4,
		Upsert:                   nil,
	}

	err4 := db.Client.FindOneAndUpdate(db.Context, filter4, update4, &result4, &ops4)

	assert.Nil(t, err4)
	assert.NotNil(t, result4)
	assert.Equal(t, 10, result4.Index)
	assert.Equal(t, "4000000001", result4.Phone)

}

func TestDbClient_PartialUpdate(t *testing.T) {
	PrepareData(t, 10, "PartialUpdate")
	updatePerson := PartialPerson{
		Name:    "partial name",
		Phone:   "",          // zero value with omitempty
		Address: "12/45 sky", //new field
		Friends: nil,         //nil pointer with omitempty
	}
	filter := bson.D{
		{
			Key:   "actionId",
			Value: "PartialUpdate",
		},
	}
	receiver := make([]FullPerson, 0)

	updateResult, err := db.Client.PartialUpdate(db.Context, filter, updatePerson, &receiver)
	assert.Nil(t, err)
	assert.Equal(t, int64(10), updateResult.ModifiedCount)
	for _, fPerson := range receiver {
		assert.NotEmpty(t, fPerson.Name)
		assert.Equal(t, "12/45 sky", fPerson.Address)
		assert.NotEmpty(t, fPerson.Friends)
		assert.NotEmpty(t, fPerson.Phone)
	}
}

func TestDbClient_UpdateMany(t *testing.T) {
	PrepareData(t, 10, "TestUpdateMany")

	filter := bson.D{{Key: "actionId", Value: "TestUpdateMany"}}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "phone", Value: "0000000001"},
		}},
	}
	t.Log("Update Many with return data is nil")
	var result []Person
	response, err := db.Client.UpdateMany(db.Context, filter, update, &result, nil)
	assert.NotEmpty(t, response)
	assert.Nil(t, err)
	assert.Equal(t, int64(10), response.ModifiedCount)
	assert.Empty(t, response.Documents)
	assert.Empty(t, result)

	t.Log("Update Many with return data is true")

	filter = bson.D{{Key: "actionId", Value: "TestUpdateMany"}}
	update = bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "phone", Value: "0000000003"},
		}},
	}
	returnedDocument, upsert := true, false
	opts := &options.UpdateOptions{Upsert: &upsert}

	var result2 []Person
	response, err = db.Client.UpdateMany(db.Context, filter, update, &result2, &returnedDocument, opts)
	var docEntities []bson.Raw
	err = document.Entities(response.Documents, &docEntities)
	assert.NotEmpty(t, response)
	assert.Nil(t, err)
	assert.Equal(t, int64(10), response.ModifiedCount)
	assert.Equal(t, 10, len(response.Documents))

	for _, item := range docEntities {
		assert.Equal(t, "0000000003", item.Lookup("phone").StringValue())
	}

	for _, item := range result2 {
		assert.Equal(t, "0000000003", item.Phone)
	}

	t.Log("Update many with adding new doc by upsert = true and document not found.")

	filter = bson.D{{Key: "actionId", Value: "TestUpdateMany_NotFound"}}
	newUUID, _ := uuid.NewRandom()
	update = bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "phone", Value: "0000000004"},
		}},
		{Key: "$setOnInsert", Value: bson.D{
			{Key: "_id", Value: newUUID},
		}},
	}
	returnedDocument, upsert = true, true
	opts = &options.UpdateOptions{Upsert: &upsert}

	var result3 []Person
	response, err = db.Client.UpdateMany(db.Context, filter, update, &result3, &returnedDocument, opts)
	err = document.Entities(response.Documents, &docEntities)
	assert.NotEmpty(t, response)
	assert.Nil(t, err)
	assert.Equal(t, int64(1), response.ModifiedCount)
	assert.Equal(t, 1, len(response.Documents))
	assert.Equal(t, 1, len(result3))

	assert.Equal(t, "0000000004", docEntities[0].Lookup("phone").StringValue())
	assert.Equal(t, "0000000004", result3[0].Phone)

}

func TestFindOneAndReplace(t *testing.T) {

	requested, _, _ := PrepareData(t, 10, "TestFindOneAndReplace")

	t.Log("Replace with upsert and return newdoc = true and the document already on DB")
	opts1 := options.FindOneAndReplace().SetUpsert(true).SetReturnDocument(options.After)

	replaceDoc1 := requested[0]
	replaceDoc1.Name = "Replace 1"
	//newId, _ := uuid.NewRandom()
	//replaceDoc1.Id = &newId

	filter1 := bson.D{
		{
			Key:   "_id",
			Value: requested[0].Id,
		},
	}

	var result1 Person
	db.Client.FindOneAndReplace(db.Context, filter1, replaceDoc1, &result1, opts1)
	assert.True(t, true, cmp.Equal(replaceDoc1, result1))

	t.Log("Replace with upsert = true and return new document and the replace document is not on DB")
	newPerson := Person{
		Id:       nil,
		Index:    0,
		Name:     "New Person",
		Phone:    "00000",
		ActionId: "TestFindOneAndReplace",
		Friends: &[]Friend{
			{
				ID:    nil,
				Index: 0,
				Name:  "New Friend",
				Phone: "222222",
			},
		},
	}
	var result2 Person
	db.Client.FindOneAndReplace(db.Context, filter1, newPerson, &result2, opts1)
	assert.True(t, true, cmp.Equal(newPerson, result2))

	t.Log("Replace with upsert = false and return new document and the replace document is not on DB")
	newId, _ := uuid.NewRandom()
	filter2 := bson.D{
		{
			Key:   "_id",
			Value: &newId,
		},
	}
	newPerson2 := Person{
		Id:       nil,
		Index:    0,
		Name:     "New Person",
		Phone:    "00000",
		ActionId: "TestFindOneAndReplace",
		Friends: &[]Friend{
			{
				ID:    nil,
				Index: 0,
				Name:  "New Friend",
				Phone: "222222",
			},
		},
	}
	var result3 Person
	err := db.Client.FindOneAndReplace(db.Context, filter2, newPerson2, &result3, options.FindOneAndReplace().SetReturnDocument(options.After))
	assert.NotNil(t, err)

	t.Log("Replace with upsert = true and return new document and the replace document is not on DB")
	err = db.Client.FindOneAndReplace(db.Context, filter2, newPerson2, &result3, options.FindOneAndReplace().SetUpsert(true).SetReturnDocument(options.After))
	assert.Nil(t, err)
	assert.NotNil(t, result3.Id)
	result3.Id = nil
	assert.Equal(t, cmp.Equal(newPerson2, result3), true)

	t.Log("Replace with upsert nil and return new document is nil")

	replaceDoc4 := requested[3]
	replaceDoc4.Name = "Replace 4"

	filter4 := bson.D{
		{
			Key:   "_id",
			Value: requested[3].Id,
		},
	}
	var result4 Person
	err = db.Client.FindOneAndReplace(db.Context, filter4, newPerson2, &result4, options.FindOneAndReplace().SetUpsert(true).SetReturnDocument(options.Before))
	assert.Nil(t, err)
	assert.Equal(t, cmp.Equal(requested[3], result4), true)

}

func TestCount(t *testing.T) {

	PrepareData(t, 10, "TestCount")

	filter1 := bson.D{{Key: "actionId", Value: "TestCount"}}
	response, err := db.Client.Count(db.Context, filter1)
	assert.Nil(t, err)
	assert.Equal(t, int64(10), response.Count)

	filter0 := bson.D{{Key: "actionId", Value: "NoAction"}}
	response, err = db.Client.Count(db.Context, filter0)
	assert.Nil(t, err)
	assert.Equal(t, int64(0), response.Count)
}

func TestDelete(t *testing.T) {

	PrepareData(t, 10, "TestDelete")

	filter := bson.D{{Key: "actionId", Value: "TestDelete"}}
	result, err := db.Client.Delete(db.Context, filter)

	assert.Nil(t, err)
	assert.NotNil(t, result)

	assert.Equal(t, int64(10), result.DeletedCount)
}

// PrepareData for test case.
// return the data that init.
func PrepareData(t *testing.T, numberData int, actionId string) (request []Person, response []Person, err error) {
	request = initCreateModel(numberData, actionId)
	err = db.Client.Create(db.Context, request, &response)
	assert.Nil(t, err)

	return request, response, err
}

func TestRead(t *testing.T) {

	request, _, _ := PrepareData(t, 10, "TestRead")

	filter := bson.D{{Key: "actionId", Value: "TestRead"}}

	var readResponse []Person
	err := db.Client.Find(db.Context, filter, &readResponse)

	assert.Nil(t, err)
	assert.NotEmpty(t, readResponse)
	assert.Equal(t, cmp.Equal(request, readResponse), true)
}

func TestReadEmpty(t *testing.T) {

	PrepareData(t, 10, "TestReadEmpty")

	filter := bson.D{{Key: "actionId", Value: "TestReadNothing"}}
	var response []Person
	err := db.Client.Find(db.Context, filter, &response)
	assert.Nil(t, err)
	assert.Empty(t, response)

}

//
func TestReadProjection(t *testing.T) {

	PrepareData(t, 10, "TestReadProjection")

	filter := bson.D{{Key: "actionId", Value: "TestReadProjection"}}
	projection := bson.D{
		{Key: "phone", Value: 0},
		{Key: "_id", Value: 0},
		{Key: "friends.friendId", Value: 0},
		{Key: "friends.phone", Value: 0},
	}
	var response []Person

	readResponseError := db.Client.Find(db.Context, filter, &response, options.Find().SetProjection(projection))

	assert.Nil(t, readResponseError)
	assert.NotEmpty(t, response)

	for _, response := range response {
		assert.Nil(t, response.Id)
		// assert.Equal(t, "Thanh", response.Name)
		assert.Empty(t, response.Phone) // string type is not a pointer => can't be nil
		assert.NotNil(t, response.Friends)
		for _, friend := range *response.Friends {
			assert.Nil(t, friend.ID)
			assert.Empty(t, friend.Phone)
		}
	}

}

func TestReadSort(t *testing.T) {

	PrepareData(t, 10, "TestReadSort")

	filter := bson.D{{Key: "actionId", Value: "TestReadSort"}}

	// sort parent
	sort := bson.D{
		{Key: "index", Value: 1},
		{Key: "friends.index", Value: -1},
	} //1 asc -1 desc

	var response []Person
	readResponseError := db.Client.Find(db.Context, filter, &response, options.Find().SetSort(sort))

	assert.Nil(t, readResponseError)
	assert.NotEmpty(t, response)
	currentIndex := -1

	for _, data := range response {
		if currentIndex != -1 {
			assert.LessOrEqual(t, currentIndex, data.Index)
			assert.True(t, data.Index > currentIndex)
		}
		currentIndex = data.Index

		assert.NotNil(t, data.Friends)

		friendIndex := -1

		for _, friend := range *data.Friends {
			if friendIndex != -1 {
				assert.LessOrEqual(t, friend.Index, friendIndex)
			}
			friendIndex = friend.Index
		}
	}

}

func TestReadSkip(t *testing.T) {

	PrepareData(t, 10, "TestReadSkip")

	filter := bson.D{{Key: "actionId", Value: "TestReadSkip"}}

	sort := bson.D{{Key: "index", Value: 1}}
	skipVal := int64(2)
	var response []Person
	err := db.Client.Find(db.Context, filter, &response, options.Find().SetSort(sort).SetSkip(skipVal))
	assert.Nil(t, err)
	assert.NotEmpty(t, response)

	assert.Equal(t, 8, len(response))

	for _, data := range response {
		assert.LessOrEqual(t, 2, data.Index)
	}

}

//
func TestReadLimit(t *testing.T) {

	PrepareData(t, 10, "TestReadLimit")

	filter := bson.D{{Key: "actionId", Value: "TestReadLimit"}}

	sort := bson.D{{Key: "index", Value: 1}}

	var response []Person
	litmit := int64(5)
	err := db.Client.Find(db.Context, filter, &response, options.Find().SetSort(sort).SetLimit(litmit))
	assert.Nil(t, err)
	assert.NotEmpty(t, response)

	assert.Equal(t, int(litmit), len(response))

}

func TestReadSkipLimit(t *testing.T) {

	PrepareData(t, 10, "TestReadSkipLimit")

	filter := bson.D{{Key: "actionId", Value: "TestReadSkipLimit"}}
	sort := bson.D{{Key: "index", Value: 1}} // asc

	var response []Person
	err := db.Client.Find(db.Context, filter, &response, options.Find().SetSort(sort).SetSkip(5).SetLimit(3))

	assert.Nil(t, err)
	assert.NotEmpty(t, response)

	assert.LessOrEqual(t, 3, len(response))
	minIndex := 5
	for _, data := range response {
		assert.LessOrEqual(t, minIndex, data.Index)
	}

}

func TestAggregate(t *testing.T) {

	request, _, _ := PrepareData(t, 10, "TestAggregate")

	t.Log("Test Aggregate with sorting and filter")

	filter := bson.D{{Key: "actionId", Value: "TestAggregate"}}
	stage1 := bson.D{{Key: "$match", Value: filter}}

	sortD := bson.D{{Key: "index", Value: -1}}
	stage2 := bson.D{{Key: "$sort", Value: sortD}}

	stages := []bson.D{stage1, stage2}
	var result []Person
	err := db.Client.Aggregate(db.Context, stages, &result)
	if err != nil {
		fmt.Println(err)
	}

	sort.Slice(request, func(i, j int) bool {
		return request[i].Index > request[j].Index // order with desc which following the aggregate $sort = -1
	})

	assert.Equal(t, true, cmp.Equal(request, result))

	t.Log("Test Aggregate with group, sum, size")

	groupCountM := bson.D{
		{
			"$group", bson.M{
				"_id": "$name",
				"numberFriends": bson.M{
					"$sum": bson.M{
						"$size": "$friends",
					},
				},
			},
		},
	}

	stages2 := []bson.D{stage1, groupCountM}
	var result2 []PersonGroup

	err = db.Client.Aggregate(db.Context, stages2, &result2)

	assert.Nil(t, err)
	assert.NotEmpty(t, result2)

	for _, person := range result2 {
		assert.Equal(t, person.Name, fmt.Sprintf("Thanh%d", person.NumberFriends)) //Check logic of preparing data to see why.
	}
}

//
func TestAggregateEmptyResult(t *testing.T) {
	PrepareData(t, 10, "TestAggregateEmptyResult")

	filter := bson.D{{Key: "actionId", Value: "akjsbdfkjalsikj"}}
	stage1 := bson.D{{Key: "$match", Value: filter}}

	sort := bson.D{{Key: "phone", Value: 1}}
	stage2 := bson.D{{Key: "$sort", Value: sort}}

	stages := []bson.D{stage1, stage2}

	var response []Person

	err := db.Client.Aggregate(db.Context, stages, &response)
	assert.Nil(t, err)
	assert.Empty(t, response)

}

func TestMongoDBClient_Collections(t *testing.T) {
	response, err := db.Client.Collections(db.Context, "test") //test.abc test.*
	assert.Nil(t, err)
	assert.NotNil(t, response)
}

func TestCreateMultiTenantCollections(t *testing.T) {
	t.Run("Normal case without any template table", func(t *testing.T) {
		tenantId := uuid.New()
		err := db.Client.CreateMultiTenantCollections(db.Context, tenantId)
		assert.Nil(t, err)
	})
	t.Run("Case with template table have default index", func(t *testing.T) {
		var dbForTenantFunc struct {
			Client  mongodb.IDBClient
			Context *titan.Context
		}
		natClient := titan.GetDefaultClient()
		dbForTenantFunc.Client = mongodb.InitDbClient(natClient, "test_db", "testTenant.none.delete.multitenant.index.template", false)
		dbForTenantFunc.Context = titan.NewBackgroundContext()
		request := initCreateModel(1, "TestCreate")[0]
		var response Person
		err := dbForTenantFunc.Client.Create(db.Context, request, &response)
		assert.Nil(t, err)
		assert.True(t, cmp.Equal(request, response))
		tenantId := uuid.New()
		err = dbForTenantFunc.Client.CreateMultiTenantCollections(dbForTenantFunc.Context, tenantId)
		assert.Nil(t, err)
		db1, err := dbForTenantFunc.Client.Collections(dbForTenantFunc.Context, "testTenant")
		assert.Nil(t, err)
		assert.NotNil(t, db1)
		assert.NotNil(t, db1.Names)
		isExistTenantTable := false
		for _, v := range db1.Names {
			if v == fmt.Sprintf("%v.%v", "testTenant", tenantId.String()) {
				isExistTenantTable = true
			}
		}
		assert.Equal(t, true, isExistTenantTable)
	})
}
