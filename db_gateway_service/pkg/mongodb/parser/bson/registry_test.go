package bson

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConvertToByteLegacy(t *testing.T) {
	originalUUID := uuid.MustParse("05a3c752-6f3a-4601-a096-d39f8a905e38")
	result, err := ConvertToByteLegacy(originalUUID)
	assert.Nil(t, err)
	reversedUUID, err := uuid.FromBytes(result)
	assert.Nil(t, err)
	assert.Equal(t, "01463a6f-52c7-a305-385e-908a9fd396a0", reversedUUID.String())
}

func TestConvertToUUIDLegacy(t *testing.T) {
	dbUUID := uuid.MustParse("01463a6f-52c7-a305-385e-908a9fd396a0")
	dbBytes, err := dbUUID.MarshalBinary()
	assert.Nil(t, err)
	result, err := ConvertToUUIDLegacy(dbBytes, 0x03)
	assert.Nil(t, err)
	assert.Equal(t, "05a3c752-6f3a-4601-a096-d39f8a905e38", result.String())
}
