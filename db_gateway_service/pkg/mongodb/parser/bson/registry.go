package bson

import (
	"fmt"
	"reflect"

	"go.mongodb.org/mongo-driver/bson/bsonrw"
	"go.mongodb.org/mongo-driver/bson/bsontype"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/bsoncodec"
)

// JAVA_LEGACY BsonBinarySubType, we are using UUID Legacy subtype to be consistent with Java code
const UUIDLegacy = 0x03

var CustomCodecRegistry = NewCustomCodecRegistryBuilder().Build()

func NewCustomCodecRegistryBuilder() *bsoncodec.RegistryBuilder {
	var tUUID = reflect.TypeOf(uuid.UUID{})

	builder := bson.NewRegistryBuilder().
		RegisterDecoder(tUUID, bsoncodec.ValueDecoderFunc(uuidDecodeCustomValue)).
		RegisterEncoder(tUUID, bsoncodec.ValueEncoderFunc(uuidEncodeCustomValue))

	return builder
}

// uuidEncodeCustomValue encode UUID to Binary with JAVA_LEGACY 0x03
func uuidEncodeCustomValue(ec bsoncodec.EncodeContext, vw bsonrw.ValueWriter, val reflect.Value) error {
	var tUUID = reflect.TypeOf(uuid.UUID{})
	if !val.IsValid() || val.Type() != tUUID {
		return bsoncodec.ValueEncoderError{Name: "uuidEncodeCustomValue", Types: []reflect.Type{tUUID}, Received: val}
	}
	uuidData := val.Interface().(uuid.UUID)
	bytes, err := ConvertToByteLegacy(uuidData)

	if err != nil {
		return bsoncodec.ValueEncoderError{Name: "uuidEncodeCustomValue", Types: []reflect.Type{tUUID}, Received: val}
	}

	return vw.WriteBinaryWithSubtype(bytes, UUIDLegacy)
}

func ConvertToByteLegacy(uuidData uuid.UUID) (bytes []byte, err error) {
	if bytes, err = uuidData.MarshalBinary(); err == nil {
		// To support the Legacy Java UUID in the old project.
		reverseBytes(bytes, 0, 8)
		reverseBytes(bytes, 8, 8)
	}
	return bytes, err
}

func ConvertToUUIDLegacy(bytes []byte, btype byte) (uuid uuid.UUID, err error) {
	if btype != UUIDLegacy {
		return uuid, fmt.Errorf("ByteSliceDecodeValue can only be used to decode subtype 0x03 for %s, got %v", bsontype.Binary, btype)
	}
	// To support the Legacy Java UUID in the old project.
	reverseBytes(bytes, 0, 8)
	reverseBytes(bytes, 8, 8)

	err = uuid.UnmarshalBinary(bytes)

	return uuid, err
}

func uuidDecodeCustomValue(ec bsoncodec.DecodeContext, vr bsonrw.ValueReader, val reflect.Value) error {
	var tUUID = reflect.TypeOf(uuid.UUID{})
	if !val.IsValid() || val.Type() != tUUID {
		return bsoncodec.ValueDecoderError{Name: "uuidDecodeCustomValue", Types: []reflect.Type{tUUID}, Received: val}
	}
	data, subtype, err := vr.ReadBinary()
	if err != nil {
		return err
	}

	uuid, err := ConvertToUUIDLegacy(data, subtype)

	if err != nil {
		return err
	}
	val.Set(reflect.ValueOf(uuid))

	return nil
}

func reverseBytes(s []byte, start int, length int) {
	swap := reflect.Swapper(s)
	for left, right := start, start+length-1; left < right; left, right = left+1, right-1 {
		swap(left, right)
	}
}
