package bson

import (
	"fmt"
	"reflect"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
)

func interfaceSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)

	if s.Kind() != reflect.Slice {
		panic("InterfaceSlice() given a non-slice type")
	}

	ret := make([]interface{}, s.Len())

	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret
}

// ToBytesArrays using MarshalWithRegistry to convert data into [][]byte
// data Type can be Struct, primitive.D, primitive.M
func ToBytesArrays(data interface{}) ([][]byte, error) {
	array := interfaceSlice(data)

	documents := make([][]byte, 0)

	for _, item := range array {
		if err := canConvertToByte(item); err != nil {
			return nil, err
		}

		bsonItem, err := Byte(item)
		if err != nil {
			return nil, err
		}
		documents = append(documents, bsonItem)
	}

	return documents, nil
}

// canConvertToByte check if the interface can be converted to Byte with MarshalWithRegistry
func canConvertToByte(data interface{}) error {
	dataType := reflect.TypeOf(data)
	if dataType.Kind() == reflect.Struct {
		return nil
	}
	// Need to check if the data is primitive.D or primitive.M which is a slice but still can be MarshalWithRegistry
	if _, ok := data.(primitive.D); ok {
		return nil
	}
	if _, ok := data.(primitive.M); ok {
		return nil
	}
	if _, ok := data.(bson.Raw); ok {
		return nil
	}
	return fmt.Errorf("your data type is %s. The expected item type is struct or primitive.*", dataType.Kind())
}

// ToBytesArray using MarshalWithRegistry to convert data into [][]byte
// data Type can be Struct, primitive.D, primitive.M
func ToBytesArray(data ...interface{}) ([][]byte, error) {
	var documents = make([][]byte, len(data))

	for i, item := range data {
		if err := canConvertToByte(item); err != nil {
			return nil, err
		}
		bsonItem, err := Byte(item)
		if err != nil {
			return nil, err
		}
		documents[i] = bsonItem
	}

	return documents, nil
}

// Convert interface into byte before communicate with database gateway
func Byte(data interface{}) ([]byte, error) {
	return bson.MarshalWithRegistry(CustomCodecRegistry, data)
}

//ToEntity convert byte into BSON Document
func ToEntity(data []byte, receive interface{}) error {
	if len(data) == 0 {
		return nil
	}
	itemType := reflect.TypeOf(receive)
	if itemType.Kind() != reflect.Ptr {
		return fmt.Errorf("your item type is %s. The expected item type is pointer of struct", itemType.Kind())
	}

	err := bson.UnmarshalWithRegistry(CustomCodecRegistry, data, receive)
	return err
}

func ToBsonD(data interface{}) (bson.D, error) {
	bytes, err := Byte(data)
	if err != nil {
		return nil, err
	}
	var receiver bson.D
	err = ToEntity(bytes, &receiver)
	if err != nil {
		return nil, err
	}
	return receiver, nil
}
