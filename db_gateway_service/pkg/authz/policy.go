package authz

import (
	"fmt"

	"github.com/google/uuid"
	bson2 "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"
	"gitlab.com/silenteer-oss/titan"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Permission string

const (
	READ_SELF_DATA  Permission = "ReadSelfData"
	WRITE_SELF_DATA Permission = "WriteSelfData"

	READ_ALL_DATA  Permission = "ReadAllData"
	WRITE_ALL_DATA Permission = "WriteAllData"
)

type Policy struct {
	Collection string
	Field      string
	Group      Group
	Read       Permission
	Write      Permission
}

func (p *Policy) addConditionToFilter(ctx *titan.Context, filter []byte) []byte {
	group := getGroup(ctx)
	switch p.Read {
	case READ_SELF_DATA:
		if group == CARE_PROVIDER {
			ctxFilter, _ := AddCareProviderConditionToFilter(filter, p.Field, ctx.UserInfo().CareProviderUUID())
			// should handle return  error in here
			return ctxFilter
		}
		if group == CARE_CONSUMER {
			ctxFilter, _ := AddCareConsumerConditionToFilter(filter, p.Field, ctx.UserInfo().UserId, ctx.UserInfo().ExternalUserId)
			// should handle return error in here
			return ctxFilter
		}
	}

	return filter
}

func (p *Policy) addConditionToFilterDelete(ctx *titan.Context, filter []byte) []byte {
	group := getGroup(ctx)
	switch p.Write {
	case WRITE_SELF_DATA:
		if group == CARE_PROVIDER {
			ctxFilter, _ := AddCareProviderConditionToFilter(filter, p.Field, ctx.UserInfo().CareProviderUUID())
			// should handle return  error in here
			return ctxFilter
		}
		if group == CARE_CONSUMER {
			ctxFilter, _ := AddCareConsumerConditionToFilter(filter, p.Field, ctx.UserInfo().UserId, ctx.UserInfo().ExternalUserId)
			// should handle return error in here
			return ctxFilter
		}
	case WRITE_ALL_DATA:
		return filter
	}
	return filter
}

func (p *Policy) checkIntegrityForWriteDocument(ctx *titan.Context, docInBytes []byte) error {
	var doc bson.M

	bson2.ToEntity(docInBytes, &doc)

	value, ok := doc[p.Field]
	if !ok {
		return &titan.CommonException{
			Status:  401,
			Message: fmt.Sprintf("missing %s in document", p.Field),
		}
	}

	switch p.Write {
	case WRITE_ALL_DATA:
		return nil
	case WRITE_SELF_DATA:
		bin := value.(primitive.Binary)

		//Must copy binData instead of use origin bindata
		bbytes := make([]byte, 0, len(bin.Data))
		bbytes = append(bbytes, bin.Data...)

		uuid, err := bson2.ConvertToUUIDLegacy(bbytes, 0x03)
		if err != nil {
			return err
		}

		group := getGroup(ctx)
		switch group {
		case CARE_PROVIDER:
			if uuid.String() != ctx.UserInfo().CareProviderUUID().String() {
				return &titan.CommonException{
					Status:  401,
					Message: fmt.Sprintf("%s=%s in document is not authority", p.Field, uuid.String()),
				}
			}
			return nil
		case CARE_CONSUMER:
			if uuid.String() != ctx.UserInfo().UserId.String() || uuid.String() != ctx.UserInfo().ExternalUserId.String() {
				return &titan.CommonException{
					Status:  401,
					Message: fmt.Sprintf("%s=%s in document is not authority", p.Field, uuid.String()),
				}
			}
			return nil
		}
		return &titan.CommonException{
			Status:  401,
			Message: fmt.Sprintf("unauthorization group %s", group),
		}
	}
	return &titan.CommonException{
		Status:  401,
		Message: fmt.Sprintf("Require permission to write, got %s", p.Write),
	}
}

func (p *Policy) checkIntegrityForUpdateDocument(ctx *titan.Context, docInBytes []byte) error {
	group := getGroup(ctx)

	switch p.Write {
	case WRITE_ALL_DATA:
		return nil
	case WRITE_SELF_DATA:
		var doc bson.D
		bson2.ToEntity(docInBytes, &doc)

		for _, e := range doc {
			if e.Key == "$set" {
				fields, ok := e.Value.(bson.D)
				if !ok {
					continue
				}
				for _, updating := range fields {
					if updating.Key == p.Field {

						bin := updating.Value.(primitive.Binary)

						//Must copy binData instead of use origin bindata
						bbytes := make([]byte, 0, len(bin.Data))
						bbytes = append(bbytes, bin.Data...)

						uuid, err := bson2.ConvertToUUIDLegacy(bbytes, 0x03)
						if err != nil {
							return err
						}

						switch group {
						case CARE_PROVIDER:
							if uuid.String() != ctx.UserInfo().CareProviderUUID().String() {
								return &titan.CommonException{
									Status:  401,
									Message: fmt.Sprintf("%s is not authority to override with %s", p.Field, uuid.String()),
								}
							}
							return nil
						case CARE_CONSUMER:
							if uuid.String() != ctx.UserInfo().UserId.String() {
								return &titan.CommonException{
									Status:  401,
									Message: fmt.Sprintf("%s is not authority to override with %s", p.Field, uuid.String()),
								}
							}
							return nil
						}
						return &titan.CommonException{
							Status:  401,
							Message: fmt.Sprintf("unauthorization group %s", group),
						}
					}
				}
			}
		}
		return nil
	}
	return &titan.CommonException{
		Status:  401,
		Message: fmt.Sprintf("Require permission to write, got %s", p.Write),
	}
}

func (p *Policy) checkIntegrityForWriteDocuments(ctx *titan.Context, docsInBytes [][]byte) error {
	for _, doc := range docsInBytes {
		err := p.checkIntegrityForWriteDocument(ctx, doc)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *Policy) GetAggregateFilter(ctx *titan.Context) []byte {
	if p.Read == READ_ALL_DATA {
		return nil
	}

	group := getGroup(ctx)
	switch group {
	case CARE_PROVIDER:
		f := bson.M{
			"$match": bson.M{
				p.Field: ctx.UserInfo().CareProviderUUID(),
			},
		}
		filterInBytes, _ := bson2.Byte(f)
		return filterInBytes
	case CARE_CONSUMER:
		f := bson.M{
			"$match": bson.M{
				"$or:": bson.D{
					{Key: p.Field, Value: parseUUID(ctx.UserInfo().ExternalUserId.String())},
					{Key: p.Field, Value: ctx.UserInfo().UserUUID()},
				},
			},
		}
		filterInBytes, _ := bson2.Byte(f)
		return filterInBytes
	}
	return nil
}

func parseUUID(id string) *uuid.UUID {
	uuid, _ := uuid.Parse(id)
	return &uuid
}
