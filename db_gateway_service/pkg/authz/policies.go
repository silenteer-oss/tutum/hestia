package authz

import (
	"fmt"

	"gitlab.com/silenteer-oss/titan"
)

type Policies []Policy

func NewPolicies() Policies {
	return make([]Policy, 0)
}

func (p Policies) FindByGroup(group Group) *Policy {
	for i := 0; i < len(p); i++ {
		if p[i].Group == group {
			return &p[i]
		}
	}
	return nil
}

type ColPolicies map[string]Policies // [collection][]Policy

func NewColPolicies() ColPolicies {
	return make(map[string]Policies, 0)
}

func (col ColPolicies) Add(p Policy) {
	_, ok := col[p.Collection]
	if !ok {
		col[p.Collection] = NewPolicies()
	}
	col[p.Collection] = append(col[p.Collection], p)
}

type DbPolicies map[string]ColPolicies // [database name][Collection]
func NewDbPolicies() DbPolicies {
	return make(map[string]ColPolicies, 0)
}

func (db DbPolicies) Add(dbName string, p Policy) {
	if _, ok := db[dbName]; !ok {
		db[dbName] = NewColPolicies()
	}
	db[dbName].Add(p)
}

func (dbp DbPolicies) FindPolicy(ctx *titan.Context, db string, coll string) (*Policy, error) {
	group := getGroup(ctx)

	msg := fmt.Sprintf("DB: %s Coll: %s Group: %s", db, coll, group)
	collectionPolicies, ok := dbp[db]
	if !ok {
		return nil, &titan.CommonException{
			Status:  401,
			Message: fmt.Sprintf("unauthorized, %s, no policy found", msg),
		}
	}
	pols, ok := collectionPolicies[coll]
	if !ok {
		return nil, &titan.CommonException{
			Status:  401,
			Message: fmt.Sprintf("unauthorized, %s, no policy found", msg),
		}
	}
	pol := pols.FindByGroup(group)
	if pol == nil {
		return nil, &titan.CommonException{
			Status:  401,
			Message: fmt.Sprintf("unauthorized, %s, no policy found", msg),
		}
	}
	return pol, nil
}
