package authz

import (
	bson2 "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/api/dbgateway"
	"gitlab.com/silenteer-oss/titan"
	"go.mongodb.org/mongo-driver/bson"
)

type Authz struct {
	DbPolicies DbPolicies // [database name][Policy]
}

func NewAuthz(dbPolicies DbPolicies) Authz {
	return Authz{DbPolicies: dbPolicies}
}

func (a *Authz) AddPolicy(dbName string, p Policy) {
	a.DbPolicies.Add(dbName, p)
}

type IsolateDataContextRequest struct {
	Action         string
	DatabaseName   string
	CollectionName string
	Filter         []byte
}

func (a *Authz) IsolateDataContext(ctx *titan.Context, rq IsolateDataContextRequest) ([]byte, error) {
	p, err := a.findPolicy(ctx, rq.DatabaseName, rq.CollectionName)
	if err != nil {
		return rq.Filter, nil
	}

	switch rq.Action {
	case "Delete":
		filter := p.addConditionToFilterDelete(ctx, rq.Filter)
		return filter, nil
	default:
		filter := p.addConditionToFilter(ctx, rq.Filter)
		return filter, nil
	}
}

func (a *Authz) IsolateDataContextForFindOne(ctx *titan.Context, rq *dbgateway.FindOneRequest) ([]byte, error) {
	return a.IsolateDataContext(ctx, IsolateDataContextRequest{
		Action:         "FindOne",
		DatabaseName:   rq.DatabaseName,
		CollectionName: rq.CollectionName,
		Filter:         rq.Filter,
	})
}

func (a *Authz) IsolateDataContextForRead(ctx *titan.Context, rq *dbgateway.ReadRequest) ([]byte, error) {
	return a.IsolateDataContext(ctx, IsolateDataContextRequest{
		Action:         "Read",
		DatabaseName:   rq.DatabaseName,
		CollectionName: rq.CollectionName,
		Filter:         rq.Filter,
	})
}

func (a *Authz) IsolateDataContextForUpdateMany(ctx *titan.Context, rq *dbgateway.UpdateManyRequest) ([]byte, error) {

	p, err := a.findPolicy(ctx, rq.DatabaseName, rq.CollectionName)

	if err != nil {
		return rq.Filter, nil
	}

	err = p.checkIntegrityForUpdateDocument(ctx, rq.Update)
	if err != nil {
		return nil, err
	}

	return a.IsolateDataContext(ctx, IsolateDataContextRequest{
		Action:         "UpdateMany",
		DatabaseName:   rq.DatabaseName,
		CollectionName: rq.CollectionName,
		Filter:         rq.Filter,
	})
}

func (a *Authz) IsolateDataContextForCount(ctx *titan.Context, rq *dbgateway.CountRequest) ([]byte, error) {
	return a.IsolateDataContext(ctx, IsolateDataContextRequest{
		Action:         "count",
		DatabaseName:   rq.DatabaseName,
		CollectionName: rq.CollectionName,
		Filter:         rq.Filter,
	})
}

func (a *Authz) IsolateDataContextForAggregate(ctx *titan.Context, rq *dbgateway.AggregateRequest) ([][]byte, error) {

	filtersReturn := rq.Filters

	p, err := a.findPolicy(ctx, rq.DatabaseName, rq.CollectionName)
	if err != nil {
		return filtersReturn, nil
	}

	filter := p.GetAggregateFilter(ctx)
	if len(filter) > 0 {
		filtersReturn = append(filtersReturn, filter)
	}

	return filtersReturn, nil
}

func (a *Authz) IsolateDataContextForFindOneAndReplace(ctx *titan.Context, rq *dbgateway.FindOneAndReplaceRequest) ([]byte, error) {
	p, err := a.findPolicy(ctx, rq.DatabaseName, rq.CollectionName)

	if err != nil {
		return rq.Filter, nil
	}

	err = p.checkIntegrityForWriteDocument(ctx, rq.Document)
	if err != nil {
		return nil, err
	}

	return a.IsolateDataContext(ctx, IsolateDataContextRequest{
		Action:         "FindOneAndReplace",
		DatabaseName:   rq.DatabaseName,
		CollectionName: rq.CollectionName,
		Filter:         rq.Filter,
	})
}

func (a *Authz) IsolateDataContextForFindOneAndUpdate(ctx *titan.Context, rq *dbgateway.FindOneUpdateRequest) ([]byte, error) {

	p, err := a.findPolicy(ctx, rq.DatabaseName, rq.CollectionName)

	if err != nil {
		return rq.Filter, nil
	}

	err = p.checkIntegrityForUpdateDocument(ctx, rq.Update)
	if err != nil {
		return nil, err
	}

	return a.IsolateDataContext(ctx, IsolateDataContextRequest{
		Action:         "FindOneAndUpdate",
		DatabaseName:   rq.DatabaseName,
		CollectionName: rq.CollectionName,
		Filter:         rq.Filter,
	})
}

func (a *Authz) IsolateDataContextForDelete(ctx *titan.Context, rq *dbgateway.DeleteRequest) ([]byte, error) {
	return a.IsolateDataContext(ctx, IsolateDataContextRequest{
		Action:         "Delete",
		DatabaseName:   rq.DatabaseName,
		CollectionName: rq.CollectionName,
		Filter:         rq.Filter,
	})
}

func (a *Authz) CheckDataContextIsolationForCreate(ctx *titan.Context, rq *dbgateway.CreateRequest) error {
	p, err := a.findPolicy(ctx, rq.DatabaseName, rq.CollectionName)
	if err != nil {
		return nil
	}
	return p.checkIntegrityForWriteDocuments(ctx, rq.Documents)
}

func (a *Authz) findPolicy(ctx *titan.Context, db string, coll string) (*Policy, error) {
	return a.DbPolicies.FindPolicy(ctx, db, coll)
}

func (a *Authz) IsolateDataForUpdate(ctx *titan.Context, rq *dbgateway.UpdateRequest) (func(bson.D) []byte, error) {
	p, err := a.findPolicy(ctx, rq.DatabaseName, rq.CollectionName)

	originF := func(f bson.D) []byte {
		bsonFilter, _ := bson2.Byte(f)
		return bsonFilter
	}

	// always return for collection without pol
	if err != nil {
		return originF, nil
	}

	group := getGroup(ctx)

	switch p.Write {
	case WRITE_ALL_DATA:
		return originF, nil
	case WRITE_SELF_DATA:
		err = p.checkIntegrityForWriteDocuments(ctx, rq.Documents)
		if err != nil {
			return nil, err
		}

		switch group {
		case CARE_PROVIDER:
			return func(filter bson.D) []byte {
				bsonFilter, _ := bson2.Byte(filter)
				return p.addConditionToFilter(ctx, bsonFilter)
			}, nil
		case CARE_CONSUMER:
			return func(filter bson.D) []byte {
				bsonFilter, _ := bson2.Byte(filter)
				return p.addConditionToFilter(ctx, bsonFilter)
			}, nil
		}
	}

	return nil, &titan.CommonException{
		Status:  401,
		Message: "unauthorization, you need permission to perform this action",
	}
}
