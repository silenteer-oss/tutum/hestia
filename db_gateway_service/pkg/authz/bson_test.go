package authz_test

import (
	"reflect"
	"testing"

	"gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/authz"

	"github.com/stretchr/testify/assert"

	"github.com/google/uuid"
	bson2 "gitlab.com/silenteer-oss/hestia/db_gateway_service/pkg/mongodb/parser/bson"
	"go.mongodb.org/mongo-driver/bson"
)

func TestContextIsolation(t *testing.T) {
	careProviderId := uuid.New()
	filter := bson.D{{"name", "test"}}
	expectedFilter := bson.D{
		{"name", "test"},
		{"careProviderId", &careProviderId},
	}
	expectedFilterByte, _ := bson2.Byte(expectedFilter)
	filterByte, _ := bson2.Byte(filter)
	ret, _ := authz.AddCareProviderConditionToFilter(filterByte, "careProviderId", &careProviderId)
	assert.True(t, reflect.DeepEqual(ret, expectedFilterByte))
}
