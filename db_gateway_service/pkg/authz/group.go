package authz

import (
	"gitlab.com/silenteer-oss/hestia/infrastructure"
	"gitlab.com/silenteer-oss/titan"
)

type Group string

const (
	SYS_ADMIN     Group = "SYS_ADMIN"
	CARE_PROVIDER Group = "CARE_PROVIDER"
	CARE_CONSUMER Group = "CARE_CONSUMER"
	ANONYMOUS     Group = "ANONYMOUS"
)

func getGroup(ctx *titan.Context) Group {
	user := ctx.UserInfo()
	if user == nil {
		return ANONYMOUS
	}

	return getGroupByRole(user.Role)
}

func getGroupByRole(role titan.Role) Group {
	group := ANONYMOUS

	switch role {
	case infrastructure.SYS_ADMIN:
		group = SYS_ADMIN
	case infrastructure.SYSTEM: // cron job
		group = SYS_ADMIN
	case infrastructure.CARE_PROVIDER_ADMIN:
		group = CARE_PROVIDER
	case infrastructure.CARE_PROVIDER_MEMBER:
		group = CARE_PROVIDER
	case infrastructure.CARE_PROVIDER_MEMBER_PRE_SWITCH:
		group = ANONYMOUS
	case infrastructure.PATIENT:
		group = CARE_CONSUMER
	case infrastructure.PATIENT_PRE_SWITCH:
		group = ANONYMOUS
	case infrastructure.REGISTERING_USER:
		group = ANONYMOUS
	case infrastructure.APPOINTMENT_BOOKER:
		group = ANONYMOUS
	}

	return group
}

func (g *Group) IsEmpty() bool {
	return *g == ""
}
