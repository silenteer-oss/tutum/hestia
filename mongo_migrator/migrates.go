package migrate

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/silenteer-oss/titan"
	"log"
	"runtime"
	"strings"
	"sync"

	"go.mongodb.org/mongo-driver/mongo/options"

	"go.mongodb.org/mongo-driver/mongo"
)

type migrates struct {
	mux      sync.Mutex
	migrates map[string]*Migrate // map of service name and migrates
}

var globalMigrates = &migrates{
	migrates: map[string]*Migrate{},
}

func (m *migrates) getMigrate(service string) *Migrate {
	m.mux.Lock()
	defer m.mux.Unlock()

	service = strings.ToLower(strings.TrimSpace(service))

	if migrate, ok := m.migrates[service]; ok {
		return migrate
	} else {
		migrate := newMigrate(nil)
		m.migrates[service] = migrate
		return migrate
	}
}

func (m *migrates) internalRegister(up, down MigrationFunc, skip int) error {
	_, file, _, _ := runtime.Caller(skip)
	version, description, service, err := extractVersionDescription(file)
	if err != nil {
		return err
	}

	migrate := m.getMigrate(service)

	if hasVersion(migrate.migrations, version) {
		return fmt.Errorf("migration with version %v already registered", version)
	}
	migrate.migrations = append(migrate.migrations, Migration{
		Version:     version,
		Description: description,
		Up:          up,
		Down:        down,
	})
	return nil
}

func (m *migrates) Register(up, down MigrationFunc, skip int) error {
	return m.internalRegister(up, down, skip)
}

// MustRegister acts like Register but panics on errors.
func (m *migrates) MustRegister(up, down MigrationFunc, skip int) {
	if err := m.internalRegister(up, down, skip); err != nil {
		log.Fatal(err)
	}
}

// RegisteredMigrations returns all registered migrations.
func (m *migrates) RegisteredMigrations(service string) []Migration {
	migrate := m.getMigrate(service)
	ret := make([]Migration, len(migrate.migrations))
	copy(ret, migrate.migrations)
	return ret
}

// SetDatabase sets database for global migrate.
func (m *migrates) setDatabase(service string, db *mongo.Database) {
	migrate := m.getMigrate(service)
	migrate.db = db
}

// Version returns current database version.
func (m *migrates) Version(service string) (uint64, string, error) {
	migrate := m.getMigrate(service)
	return migrate.Version()
}

// Up performs "up" migration using registered migrations.
// Detailed description available in Migrate.Up().
func (m *migrates) Up(ctx context.Context, service string, n int) error {
	migrate := m.getMigrate(service)
	return migrate.Up(ctx, n)
}

// Down performs "down" migration using registered migrations.
// Detailed description available in Migrate.Down().
func (m *migrates) Down(ctx context.Context, service string, n int) error {
	migrate := m.getMigrate(service)
	return migrate.Down(ctx, n)
}

// will upgrade db to the latest version
func MigrateDb(ctx context.Context, service, uri, databaseName string) (*mongo.Database, error) {
	service = strings.ToLower(strings.TrimSpace(service))

	if _, ok := globalMigrates.migrates[service]; !ok {
		return nil, errors.New("Service not found " + service)
	}

	opt := options.Client().ApplyURI(uri)
	client, err := mongo.NewClient(opt)
	if err != nil {
		return nil, err
	}
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	db := client.Database(databaseName)
	globalMigrates.setDatabase(service, db)

	if err := globalMigrates.Up(ctx, service, AllAvailable); err != nil {
		return nil, err
	}

	titan.GetLogger().Info("Finish migration database " + databaseName)

	return db, nil
}

func Register(up, down MigrationFunc) {
	globalMigrates.MustRegister(up, down, 3)
}
