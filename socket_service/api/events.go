package api

import "encoding/json"

const DeviceExisted = "DEVICE_EXISTED"
const DeviceReady = "DEVICE_READY"
const DeviceAdded = "DEVICE_ADDED"
const DeviceLogin = "DEVICE_LOGIN"

func MarshalMessageDevice(event DeviceEvent) (string, error) {
	mess, err := json.Marshal(event)
	if err != nil {
		return "", err
	}
	return string(mess), nil
}
