package manager

// never die goroutine
func panicRecover(f func()) {
	defer func() {
		if err := recover(); err != nil {
			go panicRecover(f)
		}
	}()
	f()
}
