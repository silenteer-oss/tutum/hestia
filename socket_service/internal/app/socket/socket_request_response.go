package socket

import (
	"emperror.dev/errors"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/hestia/socket_service/configuration"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/manager"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/model"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/socket_type"
	"gitlab.com/silenteer-oss/titan"
	"logur.dev/logur"
	"net/http"
	"sync"
	"time"
)

type RequestResponseSocket struct {
	BaseSocket
	responseFuncs sync.Map
	timeOut       time.Duration
}

func NewRequestResponseSocket(session *model.Session, conn *websocket.Conn, socketManager *manager.SocketManager, logger logur.Logger) *RequestResponseSocket {
	id := uuid.New().String()
	s := &RequestResponseSocket{
		responseFuncs: sync.Map{},
		timeOut:       viper.GetDuration(configuration.SocketResponseTimeout),
	}
	logger = logur.WithFields(logger, map[string]interface{}{"sessionId": id})
	s.BaseSocket = InitBaseSocket(id, session, conn, socketManager, logger, s.OnMessage)
	socketManager.Register(s)

	return s
}

func (s *RequestResponseSocket) OnMessage(message []byte) {
	resp := titan.Response{}
	err := json.Unmarshal(message, &resp)
	if err != nil {
		titan.GetLogger().Error(fmt.Sprintf("on message unmarshal has error %+v", err))
	}

	if handlerResponse, found := s.responseFuncs.Load(resp.Headers.Get(socket_type.HeaderRequestResponseId)); found {
		handlerResponse.(func(response titan.Response))(resp)
	}
}

func (s *RequestResponseSocket) Request(request titan.Request) titan.Response {
	requestId := uuid.New().String()
	request.Headers.Set(socket_type.HeaderRequestResponseId, requestId)
	responseChan := make(chan titan.Response)
	onResponse := func(response titan.Response) {
		responseChan <- response
	}
	s.responseFuncs.Store(requestId, onResponse)

	defer func() {
		s.responseFuncs.Delete(requestId)
	}()
	jsonMessage, err := json.Marshal(request)
	if err != nil {
		s.logger.Error(fmt.Sprintf("marshal json in request has error: %+v", err))
		return titan.Response{
			StatusCode: http.StatusInternalServerError,
			Body:       []byte("invalid data"),
		}
	}

	s.Send(jsonMessage)
	select {
	case response := <-responseChan:
		return response
	case <-time.After(s.timeOut):
		s.logger.Error(fmt.Sprintf("%+v", errors.New(fmt.Sprintf("request to %v is timeout with timeout configuration is %v", request.URL, s.timeOut))))
		return titan.Response{
			StatusCode: http.StatusRequestTimeout,
			Body:       []byte("timeout"),
		}
	}
}

func BuildOpenMessage(sessionId string, deviceId string) (titan.Request, error) {
	request := titan.Request{
		URL: string(api.OpenWS),
	}
	body := api.OpenCompanionMessage{
		SessionId: sessionId,
		DeviceId:  deviceId,
	}
	jsonData, err := json.Marshal(body)
	if err != nil {
		return request, err
	}
	request.Body = jsonData
	return request, nil
}
