package socket_type

import (
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"gitlab.com/silenteer-oss/hestia/socket_service/internal/app/socket/model"
	"gitlab.com/silenteer-oss/titan"
)

type Socket interface {
	GetId() string
	Send([]byte)
	Close()
	GetSession() *model.Session
	GetConn() *websocket.Conn
	SendCloseControl() error
}

type SocketRequestResponse interface {
	Socket
	Request(request titan.Request) titan.Response
}

type RequestResponseMessage struct {
	RequestId uuid.UUID
	data      []byte
}

const HeaderRequestResponseId = "X-Request-Response-Id"
