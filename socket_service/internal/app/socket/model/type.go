package model

import (
	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/titan"
)

type Session titan.UserInfo

var TestDeviceId = uuid.MustParse("2f059632-c7b1-41b8-9a55-8a3e96e9dcfc")
