package app

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/spf13/viper"
)

func init() {
	rand.Seed(time.Now().Unix())
	viper.SetDefault("Http.SocketPort", 8095)
}

type HttpConfig struct {
	SocketPort int
}

func GetHttpConfig() *HttpConfig {
	var config HttpConfig
	err := viper.UnmarshalKey("Http", &config)
	if err != nil {
		fmt.Println(fmt.Sprintf("Unmarshal http config error %+v", err))
		os.Exit(1)
	}
	return &config
}
