package app

import (
	"gitlab.com/silenteer-oss/titan"
	"logur.dev/logur"
	"net/http"
)

type AuthService struct {
	logger              logur.Logger
	natClient           *titan.Client
	jwtCookieNames      []string
	requestUri4Validate string
}

func (srv *AuthService) validateToken(r *http.Request) (string, error) {
	// validate cookie jwt and session
	var cookieExist = false
	for i := 0; i < len(srv.jwtCookieNames); i++ {
		_, err := r.Cookie(srv.jwtCookieNames[i])
		if err == nil {
			cookieExist = true
			break
		}
	}
	if cookieExist == false {
		return "", nil // there is no jwt or jwt_patients
	}
	request, _ := titan.NewReqBuilder().
		Post(srv.requestUri4Validate).
		SetHeaders(r.Header).
		Build()
	resp, sendErr := srv.natClient.SendRequest(titan.NewBackgroundContext(), request)
	// sending error
	if sendErr != nil {
		return "", sendErr
	}
	// no response
	if resp.Body == nil || len(resp.Body) == 0 {
		return "", nil
	}

	if resp.Body != nil && len(resp.Body) > 2 {
		return string(resp.Body), nil
	}

	return "", nil
}

func (srv *AuthService) validateCompanionToken(r *http.Request) (string, error) {
	request, _ := titan.NewReqBuilder().
		Post(srv.requestUri4Validate).
		SetHeaders(r.Header).
		Build()
	resp, sendErr := srv.natClient.SendRequest(titan.NewBackgroundContext(), request)
	// sending error
	if sendErr != nil {
		return "", sendErr
	}
	// no response
	if resp.Body == nil || len(resp.Body) == 0 {
		return "", nil
	}

	if resp.Body != nil && len(resp.Body) > 2 {
		return string(resp.Body), nil
	}

	return "", nil
}

func InitAuthService(natClient *titan.Client, uri string, jwtNames []string, log logur.Logger) *AuthService {
	return &AuthService{
		logger:              log,
		natClient:           natClient,
		jwtCookieNames:      jwtNames,
		requestUri4Validate: uri,
	}
}
