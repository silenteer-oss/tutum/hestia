package context

import (
	"fmt"
	"strings"

	"gitlab.com/silenteer-oss/titan"
)

const FixtureCacheKeyPrefix = "__fixture_cache__"

func AddReference(ctx *titan.Context, key string, obj interface{}) {
	cacheKey := GetCareProviderCacheKey(ctx, key)
	cache := GetGlobalCache(ctx)
	cache.Data[cacheKey] = obj
}

func GetReference(ctx *titan.Context, key string) interface{} {
	cacheKey := GetCareProviderCacheKey(ctx, key)
	cache := GetGlobalCache(ctx)
	if val, ok := cache.Data[cacheKey]; ok {
		return val
	}
	if val, ok := cache.Data[key]; ok {
		return val
	}

	// get on other care provider also
	for k, val := range cache.Data {
		if strings.HasSuffix(k, FixtureCacheKeyPrefix+key) {
			return val
		}
	}
	return nil
}

func GetCareProviderCacheKey(ctx *titan.Context, key string) string {
	careProvider := ""
	userInfo := ctx.UserInfo()
	if userInfo != nil {
		careProvider = userInfo.CareProviderId.String()
	}
	cacheKey := fmt.Sprintf("%s%s%s", careProvider, FixtureCacheKeyPrefix, key)
	return cacheKey
}

func GetGlobalCache(ctx *titan.Context) *titan.GlobalCache {
	cache := ctx.GlobalCache()
	if cache == nil {
		panic("global cache is nil")
	}
	return ctx.GlobalCache()
}
