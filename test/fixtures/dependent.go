package fixtures

type DependentInterface interface {
	GetDependencies() []FixtureInterface
	AddDependencies(fixtures ...FixtureInterface)
}

type AbstractDependentFixture struct {
	dependencies []FixtureInterface
}

func (a *AbstractDependentFixture) GetDependencies() []FixtureInterface {
	if a.dependencies == nil {
		a.dependencies = []FixtureInterface{}
	}
	return a.dependencies
}

func (a *AbstractDependentFixture) AddDependencies(fixtures ...FixtureInterface) {
	if a.dependencies == nil {
		a.dependencies = []FixtureInterface{}
	}
	a.dependencies = append(a.dependencies, fixtures...)
}

func newAbstractDependentFixture() DependentInterface {
	return &AbstractDependentFixture{}
}
