package test

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"gitlab.com/silenteer-oss/hestia/test/data"

	"gitlab.com/silenteer-oss/hestia/test/fixtures"

	"github.com/google/uuid"

	infra "gitlab.com/silenteer-oss/hestia/infrastructure"
	"gitlab.com/silenteer-oss/titan"
)

var sysadminCtx *titan.Context
var sysadminUserInfo *titan.UserInfo

func init() {
	sysadminUserInfo = &titan.UserInfo{
		UserId:         "b7b2f26d-0dc9-4d24-a829-e46c08b12161", //no one, does not exist
		Role:           infra.SYS_ADMIN,
		CareProviderId: "",
	}
	sysadminCtx = titan.NewContext(context.WithValue(context.Background(), titan.XUserInfo, sysadminUserInfo))
}

func (suite *TestSuite) GatAllCareProviders() []data.CareProviderInterface {
	return fixtures.GetAllCareProviders(suite.fixturesContext)
}

func (suite *TestSuite) GetCareProvider(key string) data.CareProviderInterface {
	return fixtures.GetCareProvider(suite.fixturesContext, key)
}

func (suite *TestSuite) GetCareProviderById(id uuid.UUID) data.CareProviderInterface {
	return fixtures.GetCareProviderById(suite.fixturesContext, id)
}

func (suite *TestSuite) LoginAsSysadmin(t *testing.T) *titan.Context {
	return titan.NewContext(context.WithValue(context.Background(), titan.XUserInfo, sysadminUserInfo))
}

func (suite *TestSuite) LoginAsDoctor(t *testing.T, care data.CareProviderInterface, doctor data.DoctorInterface) *titan.Context {
	t.Helper()
	err := suite.validateCareProviderExist(care)
	if err != nil {
		t.Fatalf("failed to log in as doctor: %+v", err)
	}
	return care.LoginAsDoctor(t, doctor)
}

func (suite *TestSuite) LoginAsNurse(t *testing.T, care data.CareProviderInterface, nurse data.NurseInterface) *titan.Context {
	t.Helper()
	err := suite.validateCareProviderExist(care)
	if err != nil {
		t.Fatalf("failed to log in as nurse: %v", err)
	}
	return care.LoginAsNurse(t, nurse)
}

func (suite *TestSuite) LoginAsAdmin(t *testing.T, care data.CareProviderInterface) *titan.Context {
	t.Helper()
	err := suite.validateCareProviderExist(care)
	if err != nil {
		t.Fatalf("failed to log in as admin: %v", err)
	}
	return care.LoginAsAdmin(t)
}

// PATIENT_PRE_SWITCH
func (suite *TestSuite) LoginAsCustomer(t *testing.T, care data.CareProviderInterface, patient data.PatientInterface) *titan.Context {
	t.Helper()
	err := suite.validateCareProviderExist(care)
	if err != nil {
		t.Fatalf("failed to log in as customer: %v", err)
	}
	return care.LoginAsCustomer(t, patient)
}

func (suite *TestSuite) LoginAsPatient(t *testing.T, care data.CareProviderInterface, patient data.PatientInterface) *titan.Context {
	t.Helper()
	err := suite.validateCareProviderExist(care)
	if err != nil {
		t.Fatalf("failed to log in as patient: %v", err)
	}
	return care.LoginAsPatient(t, patient)
}

func (suite *TestSuite) LoginAsPatientObjectWithRole(t *testing.T, care data.CareProviderInterface, patient data.PatientInterface, role string) *titan.Context {
	t.Helper()
	err := suite.validateCareProviderExist(care)
	if err != nil {
		t.Fatalf("failed to log in as patient: %v", err)
	}
	return care.LoginAsPatientWithRole(t, patient, role)
}

func (suite *TestSuite) isCareProviderExist(care data.CareProviderInterface) bool {
	for _, provider := range suite.GatAllCareProviders() {
		if care.GetId() == provider.GetId() {
			return true
		}
	}
	return false
}

func (suite *TestSuite) validateCareProviderExist(care data.CareProviderInterface) error {
	if !suite.isCareProviderExist(care) {
		return errors.New(fmt.Sprintf("care provider does not exist id=%s, name=%s", care.GetId(), care.GetName()))
	}
	return nil
}
