package test_test

import (
	"testing"

	"github.com/brianvoe/gofakeit/v5"

	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/hestia/test/data"

	"github.com/spf13/viper"

	titan_test "gitlab.com/silenteer-oss/titan/test"

	"gitlab.com/silenteer-oss/hestia/test/fixtures"

	"gitlab.com/silenteer-oss/hestia/test"

	"github.com/stretchr/testify/suite"
	"gitlab.com/silenteer-oss/titan"
)

var careProvider *CareProvider
var patient *Patient
var doctor *Doctor
var nurse *Nurse

func init() {
	viper.SetDefault(titan.LoggingLevel, "error")
	careProvider = newCareProvider()
}

type ExampleFixture struct {
	fixtures.AbstractDependentFixture
	fixtures.AbstractReferenceFixture
}

func (e *ExampleFixture) SetUp(ctx *titan.Context) error {
	gofakeit.Seed(2)
	patient = newPatient()
	doctor = newDoctor()
	nurse = newNurse()

	e.AddReference(ctx, careProvider.GetId().String(), careProvider)
	e.AddReference(ctx, patient.PatientId.String(), patient)
	e.AddReference(ctx, doctor.AccountId.String(), doctor)
	e.AddReference(ctx, nurse.AccountId.String(), nurse)
	return nil
}

func (e *ExampleFixture) TearDown(ctx *titan.Context) error {
	ctx.Logger().Info("tear down example fixture")
	return nil
}

func NewExampleFixture() fixtures.FixtureInterface {
	return &ExampleFixture{}
}

type ExampleTestSuite struct {
	test.TestSuite
}

func TestExampleSuite(t *testing.T) {
	testServers := titan_test.NewTestServers(titan.NewServer("api.test.example"))
	s := new(ExampleTestSuite)
	s.SetTestServers(testServers).
		SetFixtures(fixtures.NewCareProviderFixture(careProvider, "test_care", nil, NewExampleFixture()))
	suite.Run(t, s)
}

func (s *ExampleTestSuite) Test_can_get_ref_values_were_set_by_fixtures() {
	care := s.GetCareProvider("test_care")
	s.NotNil(care)

	careById := s.GetCareProviderById(care.GetId())
	s.NotNil(careById)

	s.LoginAsSysadmin(s.T())

	s.LoginAsAdmin(s.T(), care)

	s.LoginAsDoctor(s.T(), care, doctor)

	s.LoginAsPatient(s.T(), care, patient)

	s.LoginAsNurse(s.T(), care, nurse)
}

type CareProvider struct {
	data.AbstractCareProvider
}

func newCareProvider() *CareProvider {
	return &CareProvider{
		AbstractCareProvider: *data.NewCareProvider(uuid.New(), "", "", uuid.New(), uuid.New(), newCareProviderPersist())}
}

type Patient struct {
	data.AbstractPatient
}

func newPatient() *Patient {
	return &Patient{
		AbstractPatient: data.AbstractPatient{
			AccountId:        uuid.MustParse(gofakeit.UUID()),
			PatientId:        uuid.MustParse(gofakeit.UUID()),
			AccountProfileId: uuid.MustParse(gofakeit.UUID()),
			PatientProfileId: uuid.MustParse(gofakeit.UUID()),
		}}
}

type Employee struct {
	data.AbstractEmployee
}

func newEmployee() data.EmployeeInterface {
	return &Employee{
		AbstractEmployee: data.AbstractEmployee{
			AccountId:  uuid.MustParse(gofakeit.UUID()),
			EmployeeId: uuid.MustParse(gofakeit.UUID()),
			ProfileId:  uuid.MustParse(gofakeit.UUID()),
		}}
}

type Doctor struct {
	Employee
}

func newDoctor() *Doctor {
	return &Doctor{
		Employee: Employee{
			AbstractEmployee: data.AbstractEmployee{
				AccountId:  uuid.MustParse(gofakeit.UUID()),
				EmployeeId: uuid.MustParse(gofakeit.UUID()),
				ProfileId:  uuid.MustParse(gofakeit.UUID()),
			},
		},
	}
}

type Nurse struct {
	Employee
}

func newNurse() *Nurse {
	return &Nurse{
		Employee: Employee{
			AbstractEmployee: data.AbstractEmployee{
				AccountId:  uuid.MustParse(gofakeit.UUID()),
				EmployeeId: uuid.MustParse(gofakeit.UUID()),
				ProfileId:  uuid.MustParse(gofakeit.UUID()),
			},
		},
	}
}

type CareProviderPersist struct {
}

func (c2 CareProviderPersist) CleanUpData(c data.CareProviderInterface) error {
	return nil
}

func (c2 CareProviderPersist) SetUpData(c data.CareProviderInterface) error {
	return nil
}

func (c2 CareProviderPersist) IsEmployeeExist(c data.CareProviderInterface, employee data.EmployeeInterface) bool {
	return true
}

func (c2 CareProviderPersist) IsPatientExist(c data.CareProviderInterface, patient data.PatientInterface) bool {
	return true
}

func newCareProviderPersist() data.CareProviderPersister {
	return CareProviderPersist{}
}
