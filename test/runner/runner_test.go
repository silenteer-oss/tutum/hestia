package runner_test

import (
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/silenteer-oss/hestia/test/runner"

	"gitlab.com/silenteer-oss/titan"

	"gitlab.com/silenteer-oss/hestia/test/fixtures"

	"github.com/stretchr/testify/require"
)

var ctx *titan.Context

func TestMain(m *testing.M) {
	ctx = titan.NewBackgroundContext()
	exitVal := m.Run()
	os.Exit(exitVal)
}

func Test_should_run_multiple_fixture_successfully(t *testing.T) {
	var err error
	f1 := newExampleFixture()
	f11 := newExampleFixture()
	f12 := newExampleFixture()
	f1.AddDependencies(f11, f12)

	f2 := newExampleFixture()
	f21 := newExampleFixture()
	f2.AddDependencies(f21)

	f3 := newExampleFixture()

	err = runner.SetUp(ctx, f1, f2, f3)
	require.Nil(t, err)
	assert.True(t, f1.isUp)
	assert.True(t, f11.isUp)
	assert.True(t, f12.isUp)

	assert.True(t, f2.isUp)
	assert.True(t, f2.isUp)

	assert.True(t, f3.isUp)

	err = runner.TearDown(ctx, f1, f2, f3)
	assert.True(t, f1.isDown)
	assert.True(t, f11.isDown)
	assert.True(t, f12.isDown)

	assert.True(t, f2.isDown)
	assert.True(t, f2.isDown)

	assert.True(t, f3.isDown)
}

func Test_should_throw_exception_on_fixture_error(t *testing.T) {
	var err error
	f1 := newFailureFixture()
	err = runner.SetUp(ctx, f1)
	assert.NotNil(t, err)

	err = runner.TearDown(ctx, f1)
	assert.NotNil(t, err)
}

type exampleFixture struct {
	fixtures.AbstractDependentFixture
	fixtures.AbstractReferenceFixture
	isUp   bool
	isDown bool
}

func (e *exampleFixture) SetUp(ctx *titan.Context) error {
	e.isUp = true
	return nil
}

func (e *exampleFixture) TearDown(ctx *titan.Context) error {
	e.isDown = true
	return nil
}

func newExampleFixture() *exampleFixture {
	return &exampleFixture{}
}

type failureFixture struct {
	fixtures.AbstractDependentFixture
	fixtures.AbstractReferenceFixture
}

func (e *failureFixture) SetUp(ctx *titan.Context) error {
	return errors.New("setup fixture error")
}

func (e *failureFixture) TearDown(ctx *titan.Context) error {
	return errors.New("teardown fixture error")
}

func newFailureFixture() *failureFixture {
	return &failureFixture{}
}
