package conn

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	defaultClient *mongo.Client
	doOnce        sync.Once
	err           error
)

func init() {
	viper.SetDefault("mongodb.uri", "mongodb://127.0.0.1:27017")
}

func CreateMongoConnection(ctx context.Context) (*mongo.Client, error) {
	mongoUri := viper.GetString("mongodb.uri")
	log.Println("mongoUri_hestia: ", mongoUri)
	if mongoUri == "" {
		panic("mongodb.uri is not set")
	}
	doOnce.Do(func() {
		options := options.Client().ApplyURI(mongoUri)
		defaultClient, err = mongo.Connect(ctx, options)
		if err != nil {
			return
		}
		go func() {
			done := make(chan os.Signal, 1)
			signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
			<-done
			_ = defaultClient.Disconnect(ctx)
		}()
	})

	return defaultClient, err
}
