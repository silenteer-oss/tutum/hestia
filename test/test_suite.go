package test

import (
	"emperror.dev/errors"
	"gitlab.com/silenteer-oss/hestia/test/fixtures"
	"gitlab.com/silenteer-oss/hestia/test/runner"
	"gitlab.com/silenteer-oss/titan/test"

	"gitlab.com/silenteer-oss/hestia/socket_service"

	"gitlab.com/silenteer-oss/titan"

	"github.com/stretchr/testify/suite"
)

// for more information please refer to testify/suite at https://gowalker.org/github.com/stretchr/testify/suite
type TestSuite struct {
	suite.Suite
	fixtures    []fixtures.FixtureInterface
	testServers test.TestServersInterface

	natsConfig         *titan.NatsConfig
	socketService      socket_service.SocketService
	enableSocketServer bool
	fixturesContext    *titan.Context
}

// This will run before before the tests in the suite are run
func (suite *TestSuite) SetupSuite() {
	suite.fixturesContext = titan.NewBackgroundContext()

	if suite.testServers != nil {
		suite.testServers.Start()
	}

	if suite.socketService == nil && suite.enableSocketServer {
		suite.socketService = socket_service.InitServer(suite.natsConfig)
		go func() {
			suite.socketService.Start()
		}()
	}

	err := suite.ResetData()
	suite.Require().Nil(err)
}

func (suite *TestSuite) TearDownSuite() {
	// clean up data
	err := suite.CleanUpData()
	suite.Require().Nil(err)

	if suite.enableSocketServer && suite.socketService != nil {
		suite.socketService.Stop()
	}

	if suite.testServers != nil {
		suite.testServers.Stop()
	}
}

func (suite *TestSuite) SetUpFixtures(fixtures ...fixtures.FixtureInterface) error {
	if len(fixtures) > 0 {
		return runner.SetUp(suite.fixturesContext, fixtures...)
	}
	return nil
}

func (suite *TestSuite) TearDownFixtures(fixtures ...fixtures.FixtureInterface) error {
	if len(fixtures) > 0 {
		return runner.TearDown(suite.fixturesContext, fixtures...)
	}
	return nil
}

func (suite *TestSuite) ResetFixtures(fixtures ...fixtures.FixtureInterface) error {
	if len(fixtures) <= 0 {
		return nil // nothing to run
	}

	err := runner.TearDown(suite.fixturesContext, fixtures...)
	if err != nil {
		return errors.WithMessage(err, "Error on Reset TearDown fixtures")
	}

	err = runner.SetUp(suite.fixturesContext, fixtures...)
	if err != nil {
		return errors.WithMessage(err, "Error on Reset SetUp fixtures")
	}

	return nil
}

//synonyms of ResetFixtures
func (suite *TestSuite) ResetData() error {
	return suite.ResetFixtures(suite.fixtures...)
}

func (suite *TestSuite) SetUpData() error {
	return suite.SetUpFixtures(suite.fixtures...)
}

func (suite *TestSuite) CleanUpData() error {
	return suite.TearDownFixtures(suite.fixtures...)
}

/*
// This will run before each test in the suite
func (suite *TestSuite) SetupTest() {
	fmt.Println("SetupTest.......")
}

// This will run right before the test starts
// and receives the suite and test names as input
func (suite *TestSuite) BeforeTest(suiteName, testName string) {
	fmt.Println(fmt.Sprintf("BeforeTest suiteName = %s, testName = %s", suiteName, testName))
}

// This will run after test finishes
// and receives the suite and test names as input
func (suite *TestSuite) AfterTest(suiteName, testName string) {
	fmt.Println(fmt.Sprintf("AfterTest suiteName = %s, testName = %s", suiteName, testName))
}

func (suite *TestSuite) TearDownTest() {
	fmt.Println("TearDownTest.......")
}

*/
