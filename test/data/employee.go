package data

import (
	"github.com/google/uuid"
)

type AbstractPatient struct {
	AccountId        uuid.UUID
	PatientId        uuid.UUID // AbstractPatient.Patient
	AccountProfileId uuid.UUID
	PatientProfileId uuid.UUID
}

func (p *AbstractPatient) GetAccountId() uuid.UUID {
	return p.AccountId
}

func (p *AbstractPatient) GetPatientId() uuid.UUID {
	return p.PatientId
}

func (p *AbstractPatient) GetPatientProfileId() uuid.UUID {
	return p.PatientProfileId
}

func (p *AbstractPatient) GetAccountProfileId() uuid.UUID {
	return p.AccountProfileId
}

func newAbstractPatient() PatientInterface {
	return &AbstractPatient{}
}

type AbstractEmployee struct {
	AccountId  uuid.UUID
	EmployeeId uuid.UUID
	ProfileId  uuid.UUID
}

func (e *AbstractEmployee) GetAccountId() uuid.UUID {
	return e.AccountId
}

func (e *AbstractEmployee) GetEmployeeId() uuid.UUID {
	return e.EmployeeId
}

func (e *AbstractEmployee) GetProfileId() uuid.UUID {
	return e.ProfileId
}

func newAbstractEmployee() EmployeeInterface {
	return &AbstractEmployee{}
}

type AbstractDoctor struct {
	AbstractEmployee
}

func newDoctor() DoctorInterface {
	return &AbstractDoctor{}
}

type AbstractNurse struct {
	AbstractEmployee
}

func newNurse() NurseInterface {
	return &AbstractNurse{}
}
