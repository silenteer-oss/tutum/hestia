package data

import (
	"testing"

	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/titan"
)

type CareProviderInterface interface {
	// profile
	GetId() uuid.UUID
	GetAdminCtx() *titan.Context
	GetName() string
	GetKey() string
	//GetAdminId() uuid.UUID

	GetEncryptedKey() string
	GetHashedId() string
	//GetAppName() string

	GetAdminUserId() uuid.UUID
	GetDeviceId() uuid.UUID

	//assertion
	LoginAsDoctor(t *testing.T, doctor DoctorInterface) *titan.Context
	LoginAsNurse(t *testing.T, nurse NurseInterface) *titan.Context
	LoginAsAdmin(t *testing.T) *titan.Context
	SwitchToAdminContext(ctx *titan.Context) *titan.Context
	LoginAsCustomer(t *testing.T, patient PatientInterface) *titan.Context
	LoginAsPatient(t *testing.T, patient PatientInterface) *titan.Context
	LoginAsPatientWithRole(t *testing.T, patient PatientInterface, role string) *titan.Context

	// persist
	CleanUpData() error
	SetUpData() error
	IsEmployeeExist(employee EmployeeInterface) bool
	IsPatientExist(patient PatientInterface) bool
}

type CareProviderPersister interface {
	CleanUpData(c CareProviderInterface) error
	SetUpData(c CareProviderInterface) error
	IsEmployeeExist(c CareProviderInterface, employee EmployeeInterface) bool
	IsPatientExist(c CareProviderInterface, patient PatientInterface) bool
}

type EmployeeInterface interface {
	GetAccountId() uuid.UUID
	GetEmployeeId() uuid.UUID
	GetProfileId() uuid.UUID
}

type DoctorInterface interface {
	EmployeeInterface
}

type NurseInterface interface {
	EmployeeInterface
}

type PatientInterface interface {
	GetAccountId() uuid.UUID
	GetPatientId() uuid.UUID
	GetPatientProfileId() uuid.UUID
	GetAccountProfileId() uuid.UUID
}
