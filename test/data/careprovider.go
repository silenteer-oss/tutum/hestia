package data

import (
	"context"
	"errors"
	"testing"

	context2 "gitlab.com/silenteer-oss/hestia/test/context"

	"gitlab.com/silenteer-oss/hestia/test/db"

	"gitlab.com/silenteer-oss/hestia/infrastructure/util"

	"github.com/google/uuid"
	infra "gitlab.com/silenteer-oss/hestia/infrastructure"
	"gitlab.com/silenteer-oss/titan"
)

type AbstractCareProvider struct {
	id           uuid.UUID
	hashedId     string
	key          string
	encryptedKey string
	name         string

	adminCtx *titan.Context

	adminUserId uuid.UUID
	deviceId    uuid.UUID

	Persister CareProviderPersister
}

func (c *AbstractCareProvider) GetAdminUserId() uuid.UUID {
	return c.adminUserId
}

func (c *AbstractCareProvider) GetDeviceId() uuid.UUID {
	return c.deviceId
}

func (c *AbstractCareProvider) GetKey() string {
	return c.key
}

func (c *AbstractCareProvider) GetEncryptedKey() string {
	return c.encryptedKey
}

func (c *AbstractCareProvider) GetHashedId() string {
	return c.hashedId
}

func (c *AbstractCareProvider) GetId() uuid.UUID {
	return c.id
}

func (c *AbstractCareProvider) GetAdminCtx() *titan.Context {
	return c.adminCtx
}

func (c *AbstractCareProvider) GetName() string {
	return c.name
}

func newCareProvider() CareProviderInterface {
	return &AbstractCareProvider{}
}

func NewCareProvider(id uuid.UUID,
	name string, key string,
	adminUserId uuid.UUID, deviceId uuid.UUID,
	persister CareProviderPersister) *AbstractCareProvider {

	//key := titan.RandomString(40)
	encryptedKey, _ := util.EncryptPBE(key, db.GlobalKey)

	adminUserInfo := NewAdminUserInfo(id, key, adminUserId)

	adminCtx := titan.NewContext(context.WithValue(context.Background(), titan.XUserInfo, adminUserInfo))

	return &AbstractCareProvider{
		id:           id,
		hashedId:     util.Hash(id.String(), key),
		key:          key,
		encryptedKey: encryptedKey,
		//appName:      appName,
		name:     name,
		adminCtx: adminCtx,

		adminUserId: adminUserId,
		deviceId:    deviceId,

		Persister: persister,
	}
}

func (c *AbstractCareProvider) LoginAsDoctor(t *testing.T, doctor DoctorInterface) *titan.Context {
	t.Helper()

	d := c.IsEmployeeExist(doctor)
	if !d {
		t.Fatalf("AbstractDoctor not found: %s", doctor.GetAccountId())
	}

	doctorInfo := CreateDoctorInfo(c, doctor.GetEmployeeId())

	return c.loginSuccess(doctorInfo)
}

func (c *AbstractCareProvider) LoginAsNurse(t *testing.T, nurse NurseInterface) *titan.Context {
	t.Helper()
	n := c.IsEmployeeExist(nurse)
	if !n {
		t.Fatalf("AbstractNurse not found %s", nurse.GetAccountId())
	}
	nurseInfo := c.createUserInfo(nurse.GetEmployeeId(), infra.CARE_PROVIDER_MEMBER, "")
	return c.loginSuccess(nurseInfo)
}

func (c *AbstractCareProvider) LoginAsAdmin(t *testing.T) *titan.Context {
	t.Helper()
	nurseInfo := c.createUserInfo(c.adminUserId, infra.CARE_PROVIDER_ADMIN, "")
	return c.loginSuccess(nurseInfo)
}

func (c *AbstractCareProvider) SwitchToAdminContext(ctx *titan.Context) *titan.Context {
	adminInfo := c.createUserInfo(c.adminUserId, infra.CARE_PROVIDER_ADMIN, "")
	return c.loginSuccess(adminInfo)
}

// PATIENT_PRE_SWITCH
func (c *AbstractCareProvider) LoginAsCustomer(t *testing.T, patient PatientInterface) *titan.Context {
	t.Helper()
	p := c.IsPatientExist(patient)
	if !p {
		t.Fatalf("AbstractPatient not found %s", patient.GetAccountId())
	}

	info := c.createUserInfo(patient.GetPatientProfileId(),
		infra.PATIENT_PRE_SWITCH,
		titan.UUID(patient.GetAccountId().String()))

	return c.loginSuccess(info)
}

func (c *AbstractCareProvider) LoginAsPatient(t *testing.T, patient PatientInterface) *titan.Context {
	t.Helper()
	p := c.IsPatientExist(patient)
	if !p {
		t.Fatalf("AbstractPatient not found %s", patient.GetAccountId())
	}
	info := c.createUserInfo(patient.GetPatientId(),
		infra.PATIENT,
		titan.UUID(patient.GetAccountId().String()))

	return c.loginSuccess(info)
}

func (c *AbstractCareProvider) LoginAsPatientWithRole(t *testing.T, patient PatientInterface, role string) *titan.Context {
	t.Helper()
	p := c.IsPatientExist(patient)
	if !p {
		t.Fatalf("AbstractPatient not found %s", patient.GetAccountId())
	}
	info := c.createUserInfo(patient.GetPatientId(),
		titan.Role(role),
		titan.UUID(patient.GetAccountId().String()))

	return c.loginSuccess(info)
}

func (c *AbstractCareProvider) createUserInfo(userId uuid.UUID, role titan.Role, externalUserId titan.UUID) *titan.UserInfo {
	return CreateUserInfo(c, userId, role, externalUserId)
}

func (c *AbstractCareProvider) CleanUpData() error {
	if c.Persister == nil {
		return errors.New("Care provider  Persister is nil ")
	}
	return c.Persister.CleanUpData(c)
}

func (c *AbstractCareProvider) SetUpData() error {
	if c.Persister == nil {
		return errors.New("Care provider Persister is nil ")
	}
	return c.Persister.SetUpData(c)
}

func (c *AbstractCareProvider) IsEmployeeExist(employee EmployeeInterface) bool {
	if c.Persister == nil {
		return false
	}
	return c.Persister.IsEmployeeExist(c, employee)
}

func (c *AbstractCareProvider) IsPatientExist(patient PatientInterface) bool {
	if c.Persister == nil {
		return false
	}
	return c.Persister.IsPatientExist(c, patient)
}

func (c *AbstractCareProvider) loginSuccess(info *titan.UserInfo) *titan.Context {
	ctx := titan.NewContext(context.WithValue(context.Background(), titan.XUserInfo, info))
	context2.AddReference(ctx, c.GetId().String(), c)
	return ctx
}
