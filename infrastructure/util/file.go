package util

import (
	"emperror.dev/errors"
	"fmt"
	"os"
)

func SaveFile(fileName string, uploadFolder string, data []byte) error {
	filePath := fmt.Sprintf("%s/%s", uploadFolder, fileName)
	if _, err := os.Stat(uploadFolder); os.IsNotExist(err) {
		err := os.MkdirAll(uploadFolder, os.ModePerm)
		if err != nil {
			return errors.New(err.Error())
		}
	}
	f, err := os.Create(filePath)
	if err != nil {
		return errors.New(err.Error())
	}
	defer f.Close()

	if _, err := f.Write(data); err != nil {
		return errors.New(err.Error())
	}
	if err := f.Sync(); err != nil {
		return errors.New(err.Error())
	}
	return nil
}
