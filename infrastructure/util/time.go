package util

import "time"

func UnixMillis(date time.Time) int64 {
	return date.UnixNano() / int64(time.Millisecond)
}

func NowUnixMillis() int64 {
	return UnixMillis(time.Now())
}

func FromUnixMillis(mili int64) time.Time {
	return time.Unix(0, mili*int64(time.Millisecond))
}
func IsBetween(from time.Time, to time.Time, checkDate time.Time) bool {
	isFrom := from.IsZero() || from.Before(checkDate) || from.Equal(checkDate)
	isTo := to.IsZero() || to.After(checkDate) || to.Equal(checkDate)
	return isFrom && isTo
}
func IsBetweenMillis(from *int64, to *int64, checkDate int64) bool {
	isFrom := from == nil || (*from-checkDate) <= 0
	isTo := to == nil || (*to-checkDate) >= 0
	return isFrom && isTo
}
