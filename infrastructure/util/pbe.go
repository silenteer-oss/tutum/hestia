package util

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"golang.org/x/crypto/pbkdf2"
	"io"
)

var (
	// ErrInvalidBlockSize indicates hash blocksize <= 0.
	ErrInvalidBlockSize = errors.New("invalid blocksize")

	// ErrInvalidPKCS7Data indicates bad input to PKCS7 pad or unpad.
	ErrInvalidPKCS7Data = errors.New("invalid PKCS7 data (empty or not padded)")

	// ErrInvalidPKCS7Padding indicates PKCS7 unpad fails to bad input.
	ErrInvalidPKCS7Padding = errors.New("invalid padding on input")
)

const SaltLength = 16
const IVLength = 16
const IterationCount = 4

// Hash data with SHA512. Converted from Java project. HmacSha512Util.java
func Hash(data string, key string) string {
	mac := hmac.New(sha512.New, []byte(key))
	mac.Write([]byte(data))
	byteValue := mac.Sum(nil)
	return base64.StdEncoding.EncodeToString(byteValue)
}

func DecryptPBE(data string, key string) (string, error) {
	in, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return "", err
	}
	salt := in[0:SaltLength]
	iv := in[SaltLength : SaltLength+IVLength]
	encrypted := in[SaltLength+IVLength:]
	dk := pbkdf2.Key([]byte(key), salt, IterationCount, 16, sha512.New) //PBEWithHmacSHA512AndAES_128 AES_128 needs 16 byte key
	return decrypt(dk, iv, encrypted)
}

func EncryptPBE(data string, secretKey string) (string, error) {
	key, salt := genSecretKey(secretKey)
	cipherText := encrypt(key, []byte(data))
	wrapperCipherText := make([]byte, SaltLength+len(cipherText))
	wrapperCipherText = append(salt, cipherText...)
	return base64.StdEncoding.EncodeToString(wrapperCipherText), nil
}

func encrypt(key []byte, plaintext []byte) []byte {

	// Create the AES cipher
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	plaintext, _ = pkcs7Pad(plaintext, block.BlockSize())

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))

	iv := ciphertext[:aes.BlockSize]

	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}
	bm := cipher.NewCBCEncrypter(block, iv)
	bm.CryptBlocks(ciphertext[aes.BlockSize:], plaintext)

	return ciphertext
}

func decrypt(key []byte, iv []byte, ciphertext []byte) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	bm := cipher.NewCBCDecrypter(block, iv)
	bm.CryptBlocks(ciphertext, ciphertext)
	ciphertext, _ = pkcs7Unpad(ciphertext, aes.BlockSize)
	if err != nil {
		return "", err
	}
	return string(ciphertext), nil
}

// pkcs7Unpad validates and unpads data from the given bytes slice.
// The returned value will be 1 to n bytes smaller depending on the
// amount of padding, where n is the block size.
func pkcs7Unpad(b []byte, blocksize int) ([]byte, error) {
	if blocksize <= 0 {
		return nil, ErrInvalidBlockSize
	}
	if len(b) == 0 {
		return nil, ErrInvalidPKCS7Data
	}
	if len(b)%blocksize != 0 {
		return nil, ErrInvalidPKCS7Padding
	}
	c := b[len(b)-1]
	n := int(c)
	if n == 0 || n > len(b) {
		return nil, ErrInvalidPKCS7Padding
	}
	for i := 0; i < n; i++ {
		if b[len(b)-n+i] != c {
			return nil, ErrInvalidPKCS7Padding
		}
	}
	return b[:len(b)-n], nil
}

// pkcs7Pad right-pads the given byte slice with 1 to n bytes, where
// n is the block size. The size of the result is x times n, where x
// is at least 1.
func pkcs7Pad(b []byte, blocksize int) ([]byte, error) {
	if blocksize <= 0 {
		return nil, ErrInvalidBlockSize
	}
	if len(b) == 0 {
		return nil, ErrInvalidPKCS7Data
	}
	n := blocksize - (len(b) % blocksize)
	pb := make([]byte, len(b)+n)
	copy(pb, b)
	copy(pb[len(b):], bytes.Repeat([]byte{byte(n)}, n))
	return pb, nil
}

func genSecretKey(keyStr string) (key []byte, salt []byte) {
	salt = make([]byte, SaltLength)
	rand.Read(salt)
	return pbkdf2.Key([]byte(keyStr), salt, 4, 16, sha512.New), salt
}
