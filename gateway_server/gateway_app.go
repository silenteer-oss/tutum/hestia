package gateway_server

import (
	"gitlab.com/silenteer-oss/hestia/gateway_server/internal/app"
	"net/http"
)

// InitGatewayServer
// Default config param:
// Http.Port: 8096
func InitGatewayServer(nodeApiHost string) GatewayServer {
	return app.NewServer(nodeApiHost)
}

type GatewayServer interface {
	Start()
	Stop()
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}
