package app

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/titan"
)

func init() {
	viper.SetDefault("Http.GatewayPort", 8096)
	viper.SetDefault("patient_api_domain", "patient.local")
	viper.SetDefault("root_url", "http://patient.local")

}

type HttpConfig struct {
	Bind        string
	GatewayPort int
}

type DomainConfig struct {
	RootUrl          string
	PatientApiDomain string
}

func GetHttpConfig() *HttpConfig {
	var config HttpConfig
	err := viper.UnmarshalKey("Http", &config)
	if err != nil {
		fmt.Printf("Unmarshal http config error %+v\n", err)
		os.Exit(1)
	}

	fmt.Printf("%+v\n", config)
	fmt.Printf("Configuration gateway_server: %v", config)
	return &config
}

func GetDomainConfig() *DomainConfig {
	config := DomainConfig{
		RootUrl:          viper.GetString("root_url"),
		PatientApiDomain: viper.GetString("patient_api_domain"),
	}
	return &config
}

func NewServer(nodeApiHost string) *gatewayServer {
	// Double set to get around viper issue of not taking Env into serialization https://github.com/spf13/viper/issues/761
	viper.SetDefault("Http.Bind", "0.0.0.0")
	viper.SetEnvPrefix("GW")
	viper.BindEnv("Http.Bind")
	viper.SetDefault("Http.Bind", viper.GetString("Http.Bind"))

	return NewGatewayServer(
		titan.GetLogger(),
		titan.GetDefaultClient(),
		GetHttpConfig(),
		GetDomainConfig(),
		nodeApiHost,
	)
}
