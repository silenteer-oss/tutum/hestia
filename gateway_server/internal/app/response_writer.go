package app

import "net/http"

//  response writer with status
type ResponseWriter struct {
	writer http.ResponseWriter
	status int
}

func (r *ResponseWriter) Write(p []byte) (int, error) {
	return r.writer.Write(p)
}

func (r *ResponseWriter) WriteHeader(status int) {
	r.status = status
	r.writer.WriteHeader(status)
}

func (r *ResponseWriter) GetStatusCode() int {
	return r.status
}
