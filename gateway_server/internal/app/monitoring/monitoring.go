package monitoring

import (
	"fmt"

	"gitlab.com/silenteer-oss/titan"
)

var connection *titan.Connection
var subscription titan.ISubscription

func Start(natClient *titan.Client) {
	config := titan.GetNatsConfig()
	logger := titan.GetLogger()
	conn, err := titan.GetDefaultServer(config, logger, "api.app.monitoring")
	if err != nil {
		logger.Warn(fmt.Sprintf("Gateway nats server initialization error %+v", err))
		return
	}
	connection = conn

	sub, err := conn.Subscribe(titan.MONITORING_CHECK, func(p *titan.Message) error {
		return natClient.Publish(titan.NewBackgroundContext(), titan.MONITORING_CHECK_REPLY, titan.DoMonitoringCheck("api.app.gateway", p))
	})
	if err != nil {
		logger.Warn(fmt.Sprintf("Gateway nats server subscribe monitoring error %+v", err))
		return
	}

	subscription = sub
	err = sub.SetPendingLimits(config.PendingLimitMsg, config.PendingLimitByte)
	if err != nil {
		logger.Warn(fmt.Sprintf("Gateway nats server SetPendingLimits error %+v", err))
	}
}

func Stop() {
	if subscription != nil {
		_ = subscription.Drain()
	}
	if connection != nil {
		connection.Close()
	}
}
