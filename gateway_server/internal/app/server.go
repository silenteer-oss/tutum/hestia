package app

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/spf13/viper"

	"gitlab.com/silenteer-oss/titan/tracing"

	"gitlab.com/silenteer-oss/hestia/gateway_server/internal/app/monitoring"

	"gitlab.com/silenteer-oss/titan/log"

	"gitlab.com/silenteer-oss/titan"

	"logur.dev/logur"
)

const (
	REQUEST_URI_4_VALIDATE  = "request.uri.4.validate"
	JWT_COOKIE_NAME         = "jwt.cookie.names"
	JWT_PATIENT_COOKIE_NAME = "jwt.patient.cookie.names"
)

func init() {
	viper.SetDefault(REQUEST_URI_4_VALIDATE, "/api/service/auth/validate")
	viper.SetDefault(JWT_COOKIE_NAME, "JWT")
	viper.SetDefault(JWT_PATIENT_COOKIE_NAME, "JWT_PATIENT")
	viper.SetDefault("subject", "api.service.gateway")
}

//`Credentials Do Not Match`
type gatewayServer struct {
	logger        logur.Logger
	httpServer    *http.Server
	natClient     *titan.Client
	httpConfig    *HttpConfig
	domainConfig  *DomainConfig
	nodeJsApiHost string
	httpClient    *http.Client
}

func NewGatewayServer(logger logur.Logger,
	natClient *titan.Client,
	httpConfig *HttpConfig,
	domainConfig *DomainConfig,
	nodeJsApiHost string) *gatewayServer {

	return &gatewayServer{
		logger:        log.WithFields(logger, map[string]interface{}{"subject": viper.GetString("subject")}),
		natClient:     natClient,
		httpConfig:    httpConfig,
		domainConfig:  domainConfig,
		nodeJsApiHost: nodeJsApiHost,
		httpClient: &http.Client{
			Timeout: time.Second * 5,
		},
	}
}

func (srv *gatewayServer) GetInfo() HttpConfig {
	return *srv.httpConfig
}

func (srv *gatewayServer) Start() {
	tracing.InitTracing("gateway")
	monitoring.Start(srv.natClient)
	defer monitoring.Stop()

	httpS := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", srv.httpConfig.Bind, srv.httpConfig.GatewayPort),
		Handler: srv,
	}

	srv.httpServer = httpS

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := httpS.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			srv.logger.Error(fmt.Sprintf("listen: %s\n", err))
			os.Exit(1)
		}
	}()
	srv.logger.Info(fmt.Sprintf("Http Server Started at %s:%d", srv.httpConfig.Bind, srv.httpConfig.GatewayPort))

	<-done
	srv.logger.Info("Gateway Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	if err := httpS.Shutdown(ctx); err != nil {
		srv.logger.Error(fmt.Sprintf("Http Server Shutdown Failed:%+v", err))
		os.Exit(1)
	}
	srv.logger.Info("Gateway Server Exited Properly")
}

func (srv *gatewayServer) Stop() {
	if srv.httpServer != nil {
		_ = srv.httpServer.Shutdown(context.Background())
	}
}

func (srv *gatewayServer) setupResponse(w *ResponseWriter) {
	w.writer.Header().Set("Access-Control-Allow-Origin", srv.domainConfig.RootUrl)
	w.writer.Header().Set("Access-Control-Allow-Credentials", "true")
	w.writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.writer.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, JWT, JWT_PATIENT, origin, pragma, cache-control, x-requested-with")
}

func (srv *gatewayServer) ServeHTTP(wt http.ResponseWriter, r *http.Request) {
	w := &ResponseWriter{
		writer: wt,
		status: 200,
	}

	srv.setupResponse(w)
	if r.Method == "OPTIONS" {
		return
	}

	//health check
	if r.URL != nil && r.URL.Path == "/api/app/gateway/health" {
		_, _ = w.Write([]byte(`{"status": "UP"}`))
		return
	}

	t := time.Now()

	// check and set header
	requestId := r.Header.Get(titan.XRequestId)
	if requestId == "" {
		requestId = titan.RandomString(6)
		r.Header.Set(titan.XRequestId, requestId)
	}

	r.Header.Set(titan.XRequestTime, strconv.FormatInt(t.UnixNano(), 10))

	reqSpan := tracing.SpanContext(&r.Header, r.URL.RawPath)
	if reqSpan != nil {
		defer reqSpan.Finish()
	}

	r.Header.Set(titan.XOrigin, strings.Split(r.Host, ":")[0])

	// security checking
	r.Header.Set(titan.XUserInfo, "")

	logger := log.WithFields(srv.logger, map[string]interface{}{
		"id":     requestId,
		"method": r.Method,
		"url":    r.RequestURI,
	})
	ctx := titan.NewBackgroundContext().WithValue(titan.XLoggerId, logger)

	logger.Debug("Gateway received http request:")
	titan.BeginRequest(logger, &RequestReader{r})
	defer titan.EndRequest(logger, w)

	user, err := srv.validateToken(ctx, r)
	if err != nil {
		logger.Error(fmt.Sprintf("Request authentication error=%+v", err))
		w.WriteHeader(500)
		return
	}

	method := r.Method
	var body []byte

	queryParams := r.URL.Query()
	xMethod := queryParams.Get("X-Method")

	if xMethod != "" {
		b := map[string]string{}
		for k := range queryParams {
			b[k] = queryParams.Get(k)
		}
		body, err = json.Marshal(b)
		if err != nil {
			logger.Error(fmt.Sprintf("Bad Request, Cannot convert query params to body, error=%+v", err))
			w.WriteHeader(400) //400 Bad Request
			return
		}
		method = strings.ToUpper(xMethod)
	} else {
		// read body data
		body, err = ioutil.ReadAll(r.Body)
		if err != nil {
			logger.Error(fmt.Sprintf("Bad Request, Cannot read body, error=%+v", err))
			w.WriteHeader(400) //400 Bad Request
			return
		}
	}

	defer r.Body.Close()

	r.Header.Set(titan.XUserInfo, user)

	// NodeJS
	if strings.Index(r.URL.Path, "/api/v2/") == 0 {
		resp, err := srv.forwardToNodeJs(r, body)

		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}
		for k, vs := range resp.Header {
			for _, v := range vs {
				w.writer.Header().Add(k, v)
			}
		}
		w.WriteHeader(resp.StatusCode)

		defer resp.Body.Close()
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}
		w.Write(data)
		return
	}
	requestURI := r.RequestURI

	request := &titan.Request{
		Method:  method,
		Headers: r.Header,
		Body:    body,
		URL:     requestURI,
	}

	res := srv.sendRequest(ctx, request)

	select {
	case <-time.After(15 * time.Second):
		logger.Error("Nats sending timeout")
		w.WriteHeader(408) // timeout
		return
	case m := <-res:
		rp := m.response
		if m.err != nil {
			if clientErr, ok := m.err.(*titan.ClientResponseError); ok {
				rp = clientErr.Response
			}
		}
		if rp == nil {
			logger.Error("Response not found")
			w.WriteHeader(404)
			return
		}

		processTime := float64(time.Since(t).Milliseconds())

		logMap := map[string]interface{}{
			"status":     rp.StatusCode,
			"elapsed_ms": processTime,
		}
		if rp.StatusCode != 200 {
			logger.Error("Gateway received response error:", logMap)
		} else {
			if processTime > 4000 {
				logger.Warn("Gateway received response:", logMap)
			} else {
				logger.Debug("Gateway received response:", logMap)
			}
		}

		//copy headers
		for key, values := range rp.Headers {
			if key == titan.XUserInfo { // security checking
				continue
			}
			for _, value := range values {
				w.writer.Header().Add(key, value)
			}
		}
		w.writer.Header().Add("Connection", "keep-alive")
		w.WriteHeader(rp.StatusCode)

		// copy body
		if rp.Body != nil {
			_, _ = w.Write(rp.Body)
		}
		return
	}
}

type SendRequestResponse struct {
	err      error
	response *titan.Response
}

func (srv *gatewayServer) sendRequest(ctx *titan.Context, request *titan.Request) <-chan SendRequestResponse {
	ctx.Logger().Debug("Gateway sending request to NATS:")

	res := make(chan SendRequestResponse, 1)
	go func(request *titan.Request) {
		defer close(res)
		resb, err := srv.natClient.SendRequest(ctx, request)
		res <- SendRequestResponse{err: err, response: resb}
	}(request)
	return res
}

func (srv *gatewayServer) forwardToNodeJs(req *http.Request, body []uint8) (*http.Response, error) {
	url := fmt.Sprintf("%s%s", srv.nodeJsApiHost, req.RequestURI)
	proxyReq, err := http.NewRequest(req.Method, url, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	for k, vs := range req.Header {
		for _, v := range vs {
			proxyReq.Header.Add(k, v)
		}
	}
	proxyReq.Header = req.Header.Clone()
	return srv.httpClient.Do(proxyReq)
}

func (srv *gatewayServer) validateToken(ctx *titan.Context, r *http.Request) (string, error) {
	// validate cookie jwt and session
	apiDomain := strings.Split(r.Host, ":")[0]
	cookie, err := r.Cookie(viper.GetString(JWT_COOKIE_NAME))

	if strings.Contains(srv.domainConfig.PatientApiDomain, strings.ToLower(apiDomain)) {
		patientCookie, _err := r.Cookie(viper.GetString(JWT_PATIENT_COOKIE_NAME))
		if patientCookie != nil && len(patientCookie.Value) > 0 {
			cookie = patientCookie
		}
		if _err != nil && err != nil {
			return "", nil // there is no jwt
		}
	}

	if cookie == nil || cookie.Value == "" || cookie.Value == "undefined" {
		return "", nil // there is no jwt
	}

	request, _ := titan.NewReqBuilder().
		Post(viper.GetString(REQUEST_URI_4_VALIDATE)).
		SetHeaders(r.Header).
		Build()
	request.Headers.Set(titan.XOrigin, strings.Split(r.Host, ":")[0])

	resp, sendErr := srv.natClient.SendRequest(ctx, request)

	// sending error
	if sendErr != nil {
		return "", sendErr
	}

	// no response
	if resp.Body == nil || len(resp.Body) == 0 {
		return "", nil
	}

	if resp.Body != nil && len(resp.Body) > 2 {
		bodyInString := string(resp.Body)
		if strings.EqualFold("null", bodyInString) {
			return "", nil
		}
		return bodyInString, nil
	}

	return "", nil
}
