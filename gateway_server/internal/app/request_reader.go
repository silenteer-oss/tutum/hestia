package app

import (
	"net/http"
)

//  response writer with status
type RequestReader struct {
	request *http.Request
}

func (r *RequestReader) GetUrl() string {
	if r.request.URL != nil {
		return r.request.URL.Path
	}
	return ""
}

func (r *RequestReader) GetHeaders() http.Header {
	return r.request.Header
}
