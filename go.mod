module gitlab.com/silenteer-oss/hestia

go 1.16

replace (
	gitlab.com/silenteer-oss/goff => gitlab.com/silenteer-oss/tutum/goff v1.1.5
	gitlab.com/silenteer-oss/titan => gitlab.com/silenteer-oss/tutum/titan v1.0.73-alpha04
)

require (
	emperror.dev/errors v0.7.0
	github.com/brianvoe/gofakeit/v5 v5.9.2
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.6
	github.com/google/uuid v1.2.0
	github.com/gorilla/websocket v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.10.1
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/tidwall/pretty v1.0.2 // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	gitlab.com/silenteer-oss/goff v1.1.5
	gitlab.com/silenteer-oss/titan v1.0.69
	go.mongodb.org/mongo-driver v1.4.3
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220405210540-1e041c57c461 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	logur.dev/logur v0.16.2
)
